.. Resampler documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _resampling_opcodes:

.. spelling:word-list::
   Resampling
   resampling
   resampler
   resamplers
   resampled
   downsampling
   upsampling

Handling of OpCodes in Resampling Components
============================================
Some components have differing sampling rates on their input and output ports, all such components can be grouped together as resamplers. Such components handle some opcodes slightly differently, namely Time and Sample Interval.

Note that a component is only considered a resampler if the sampling rate actually changes. For example; a component which splits a sequential stream of data into a parallel stream is not resampling even though there are twice as many output messages as input. Likewise a component which is framing a payload is not changing the underlying data rate. As such there is a distinction between data rate, and sampling rate, which should be observed by the application developer.


Time
~~~~
When resampling, the Timestamp should be aligned such that it relates to the first outgoing sample from the block of data currently being processed. This allows a constant point within the outgoing data to use when applying group delay correction which might have been introduced by the filters used.

If no samples have been received, or the internal buffers are empty (due to a previous Flush or Discontinuity), then the Timestamp will align to the first outgoing sample in the next output, and thus should be sent through with only the group delay modifications being applied.

When downsampling or decimating, the Timestamp requires a number of Sample Intervals (at the incoming sampling rate) to be added onto the outgoing Timestamp. The number of Sample Interval additions are set such that the Timestamp aligns to the next output sample in a decimation.

.. figure:: downsample_timestamp_alignment.svg
   :name: downsample_timestamp_alignment
   :alt: Downsample Timestamp Realignment
   :align: center

   Downsample Timestamp realignment

.. |InputData| image:: input_data.svg

The |InputData| represents the received stream of samples.

.. |ModifiedTime| image:: modified_time.svg

The |ModifiedTime| represents the adjusted time sent on the output i.e. :math:`T' = T + (N * SI)` where ``T`` is the time received via a time opcode, ``N`` is the number of samples that are required to complete a downsample / decimation and ``SI`` is the received Sample Interval value.

.. |DownsamplesData| image:: downsampled_output_data.svg

The |DownsamplesData| represents the reordered and down sampled output data.

When upsampling there is no need to realign the Timestamps, as there are multiple output samples for a single input sample. However, care should be taken to ensure that the Timestamp comes out in-between upsampling operations.

.. figure:: upsample_timestamp_alignment.svg
   :alt: Upsample Timestamp Realignment
   :align: center

   Upsample Timestamp realignment

When complex resampling (i.e. multi-stage rational or irrational), then the Timestamp should still follow the above rules, as close as is feasible without large resource utilisation. This would generally mean that for a rational resampling operation, the upsample and downsampling ratio follow the stages above sequentially. While for irrational rate changes the component developer should generally state if; resampled Timestamps are forwarded at a known timing relative to the incoming data (deterministic latency), or whether the exact timing information will be made less accurate due to the operation or resource constraints (indeterministic latency).

Sample Interval
~~~~~~~~~~~~~~~
Sample Intervals should be recalculated to correctly portray the aggregate output sampling interval. This means that for an upsampling procedure the Sample Interval is divided (i.e. a smaller time delta between 2 samples), while for a downsampling operation the Sample Interval should be multiplied.

Flush
~~~~~
A Flush opcode should cause any internal sample buffer within the component to be flushed through with zeros. In addition a Flush opcode should ensure that the component ends with no resampling operation partially completed. (i.e. on the boundary of a sample on the port with the slower sampling rate).

The completion of any partial buffers also ensures that any held Timestamps are forwarded on from the component in the order shown in :numref:`downsample_timestamp_alignment`. Also note that any held Timestamps should be updated by a multiple of the Sample Interval with the number of flushed (i.e. zero-valued) samples.

If no samples have been processed since last reset (start, Flush, Discontinuity), meaning any internal processing buffers will be at reset state, no zero samples will processed and the Flush Opcode will be forwarded immediately to the output.

Discontinuity
~~~~~~~~~~~~~
If a Discontinuity is received while there is a Timestamp being held internally (i.e. a downsampling operation is in progress). Then this Timestamp should be forwarded before the Discontinuity. This will lead to a potential timing error within any downstream components, however this is preferable to the Timestamp being lost.
