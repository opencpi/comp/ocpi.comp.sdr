-- bias_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is
  constant input_size_c : integer := input_in.data'length/2;

  signal a_real : signed(input_size_c - 1 downto 0);
  signal a_imag : signed(input_size_c - 1 downto 0);
begin

  input_out.take <= output_in.ready;

  a_imag <= signed(input_in.data((2*input_size_c)-1 downto input_size_c))
            + props_in.imaginary_value;
  a_real <= signed(input_in.data(input_size_c-1 downto 0))
            + props_in.real_value;

  output_out.data <= std_logic_vector(a_imag) & std_logic_vector(a_real)
                     when input_in.opcode = complex_short_timed_sample_sample_op_e
                     else input_in.data;

  output_out.eom         <= input_in.eom;
  output_out.valid       <= input_in.valid;
  output_out.give        <= input_in.ready;
  output_out.opcode      <= input_in.opcode;
  output_out.byte_enable <= input_in.byte_enable;
end rtl;
