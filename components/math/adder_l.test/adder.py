#!/usr/bin/env python3

# Python implementation of adder block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of adder block for verification."""

import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class Adder(ocpi_testing.Implementation):
    """Testing implementation of adder."""

    def __init__(self, negate_input_1, negate_input_2):
        """Adder initialisation.

        Args:
            negate_input_1 (bool): Negate input 1.
            negate_input_2 (bool): Negate input 2.
        """
        super().__init__(negate_input_1=negate_input_1,
                         negate_input_2=negate_input_2)

        self._input_1_multiplier = 1 if negate_input_1 is False else -1
        self._input_2_multiplier = 1 if negate_input_2 is False else -1

        self.input_ports = ["input_1", "input_2"]

        self._input_1_data = []
        self._input_2_data = []

        self._output_message_lengths = []

    def reset(self):
        """Put the implementation into the same state as at initialisation."""
        self._input_1_data = []
        self._input_2_data = []
        self._output_message_lengths = []

    def select_input(self, input_1, input_2):
        """Determine which input port should be used.

        Args:
            input_1 (str): Opcode name of the next message on the input_1 port.
            input_2 (str): Opcode name of the next message on the input_2 port.

        Returns:
            An integer of the input port to be advanced, indexed from zero.
        """
        # Pull through message if not a stream, so that both ports get a stream
        # message being the next on each port.
        if input_1 != "sample" and input_1 is not None:
            return 0
        if input_2 != "sample" and input_2 is not None:
            return 1

        # Otherwise only stream messages or no messages on port.
        if input_1 is not None:
            return 0
        if input_2 is not None:
            return 1

    def sample(self, input_1, input_2):
        """Handle input sample opcode.

        Args:
            input_1 (list): Sample data on input_1 port.
            input_2 (list): Sample data on input_2 port.

        Returns:
            Formatted messages.
        """
        if input_1 is not None:
            self._input_1_data = self._input_1_data + list(input_1)
            self._output_message_lengths.append(len(input_1))
        if input_2 is not None:
            self._input_2_data = self._input_2_data + list(input_2)

        messages = []
        while len(self._output_message_lengths) > 0:
            # Check there is enough data on both inputs for output
            if len(self._input_1_data) >= self._output_message_lengths[0] and \
                    len(self._input_2_data) >= self._output_message_lengths[0]:
                output_length = self._output_message_lengths.pop(0)

                stream_data = self._get_stream_data(output_length)
                messages.append({"opcode": "sample", "data": stream_data})

                # Remove the data from the input port buffers that has been
                # processed
                self._input_1_data = self._input_1_data[output_length:]
                self._input_2_data = self._input_2_data[output_length:]

            else:
                break

        return self.output_formatter(messages)

    def _get_stream_data(self, output_length):
        """Create output stream data.

        Args:
            output_length (int): Number of samples to process.

        Returns:
            List: Resultant output values.
        """
        input_1_data = np.array(
            self._input_1_data[0:output_length], dtype=np.int32)
        input_2_data = np.array(
            self._input_2_data[0:output_length], dtype=np.int32)

        input_1_data = input_1_data * self._input_1_multiplier
        input_2_data = input_2_data * self._input_2_multiplier

        output = input_1_data + input_2_data

        return output
