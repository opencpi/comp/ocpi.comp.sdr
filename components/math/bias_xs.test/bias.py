#!/usr/bin/env python3

# Python implementation of bias block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of bias block for verification."""

import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class Bias(ocpi_testing.Implementation):
    """Testing implementation for Bias."""

    def __init__(self, real_value, imaginary_value):
        """Initialisation of a Bias class.

        Args:
            real_value (int): Bias value added to real part of input values.
            imaginary_value (int): Bias value added to imaginary part of input values.
        """
        super().__init__(real_value=real_value, imaginary_value=imaginary_value)

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Handle incoming sample opcode.

        Args:
            values (list): Incoming sample values.

        Returns:
            Formatted messages
        """
        output_values = []
        for value in values:
            real = np.int16(value.real + self.real_value)
            imag = np.int16(value.imag + self.imaginary_value)
            output_values.append(complex(real, imag))
        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
