-- Bias_s implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

begin

  input_out.take <= output_in.ready;

  output_out.data <= std_logic_vector(signed(input_in.data) + props_in.bias)
                     when input_in.opcode = short_timed_sample_sample_op_e
                     else input_in.data;

  output_out.eom         <= input_in.eom;
  output_out.valid       <= input_in.valid;
  output_out.give        <= input_in.ready;
  output_out.opcode      <= input_in.opcode;
  output_out.byte_enable <= input_in.byte_enable;
end rtl;
