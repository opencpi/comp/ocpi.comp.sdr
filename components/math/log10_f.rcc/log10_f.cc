// Log10 implementation.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cmath>  // log10

#include "log10_f-worker.hh"

using namespace OCPI::RCC;
using namespace Log10_fWorkerTypes;

class Log10_fWorker : public Log10_fWorkerBase {
  RCCResult run(bool) {
    if (input.opCode() == Float_timed_sampleSample_OPERATION) {
      size_t data_in_input = input.sample().data().size();
      const RCCFloat *inputData = input.sample().data().data();
      RCCFloat *outputData = output.sample().data().data();

      for (size_t sample = 0; sample < data_in_input; sample++) {
        *outputData++ = log10(*inputData++);
      }

      output.setOpCode(input.opCode());
      output.sample().data().resize(data_in_input);
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Float_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Float_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleFlush_OPERATION) {
      // Pass through flush opcode
      output.setOpCode(Float_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Float_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Float_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode received");
      return RCC_FATAL;
    }
  }
};

LOG10_F_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
LOG10_F_END_INFO
