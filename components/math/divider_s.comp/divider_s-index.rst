.. divider_s documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _divider_s:


Divider (``divider_s``)
=======================
Divides two integer values together.

Design
------
Divides two short values together generating a quotient and a remainder. The division method can be selected from: truncated (C/C++ standard method), and floored (Python ``//`` operator).

The mathematical representation of the implementation is given in :eq:`divider_s_quotient-equation`.

.. math::
   :label: divider_s_quotient-equation

   z[n] = \left \lfloor \frac{x[n]}{y[n]} \right \rfloor

.. math::
   :label: divider_s_mod-equation

   r[n] = {x[n]} \space \mathbf{mod} \space {y[n]}


In :eq:`divider_s_quotient-equation` and :eq:`divider_s_mod-equation`

 * :math:`x[n]` and :math:`y[n]` are the input values.

 * :math:`z[n]` is the output values.

 * :math:`r[n]` is the remainder values.

In :eq:`divider_s_quotient-equation`, all outputs are converted to integer representations.

A block diagram representation of the implementation is given in :numref:`divider_s-diagram`.

.. _divider_s-diagram:

.. figure:: divider_s.svg
   :alt: Block diagram outlining divider implementation.
   :align: center

   Divider implementation.

Interface
---------
.. literalinclude:: ../specs/divider_s-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Numerator samples port.
   denominator: Denominator samples port.
   output: Quotient samples port.
   remainder: Remainder samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Division is only applied to the sample opcode messages. All other opcode messages received on ``input`` are passed through this component to both output ports. All messages with a non-sample opcode received on ``denominator`` are discarded.

Sample message lengths received on ``input`` are preserved on the output. Sample message lengths received on ``denominator`` are ignored.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../divider_s.hdl ../divider_s.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Divider primitive <divider-primitive>`

 * :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``divider_s`` are:

* In order to keep compatibility between HDL and RCC, division by zero does not raise any exceptions, but instead returns the following:

   * If division method is ``floor``: 0 or -1 on the Quotient port (depending upon the sign of ``input``), and ``input`` is passed as the remainder.

   * If division method is ``truncate``: 1 or -1 on the Quotient port (depending upon the sign of ``input``), and ``input`` is passed as the remainder.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
