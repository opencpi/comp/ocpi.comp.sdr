// multiword integer divide gtest
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <deque>
#include <random>

#include "../multiword_integer_divide.hh"
#include "gtest/gtest.h"
#include "multiword_integer_multiply.hh"

// g++ -I/opt/opencpi/prerequisites/gtest/include -std=c++11 -Wno-narrowing
// multiword_divide_gtest.cpp -L/opt/opencpi/prerequisites/gtest/centos7/lib/
// -lgtest -o multiword_divide_gtest
// LD_LIBRARY_PATH=/opt/opencpi/prerequisites/gtest/centos7/lib/
// ./multiword_divide_gtest
//

// Divide test.
// * Divide algorithm of the 3 cases in the algorithm.
// * Places guard values of 42 at +1 positions to check for buffer overflow.
// * Uses multiply to generate some test data: The multiply and divide
//   algorithms have no common implementation so the chances of error
//   cancellation are effectivily non-existent.

// Divide m/n where n>m: Result 0 rem m.
TEST(DivideTest, Case1) {
  uint32_t lhs[3]{100, 42, 42};
  uint32_t rhs[3]{101, 102, 42};
  uint32_t quot[3]{42, 42, 42};
  uint32_t rem[3]{42, 42, 42};

  // 100 by (2 words) -> 0 rem 100
  multiword_unsigned_divide(lhs, 1, rhs, 2, quot, rem);
  EXPECT_EQ(quot[0], 0);
  EXPECT_EQ(quot[1], 42);
  EXPECT_EQ(rem[0], 100);
  EXPECT_EQ(rem[1], 0);
}

// Divisor is single word.
TEST(DivideTest, Case2) {
  uint32_t lhs[4]{20005, 42, 42, 42};
  uint32_t rhs[4]{10, 42, 42, 42};
  uint32_t quot[4]{42, 42, 42, 42};
  uint32_t rem[4]{42, 42, 42, 42};

  // 20005 by 10 -> 2000 r 5
  multiword_unsigned_divide(lhs, 1, rhs, 1, quot, rem);
  EXPECT_EQ(quot[0], 2000);
  EXPECT_EQ(quot[1], 42);
  EXPECT_EQ(rem[0], 5);
  EXPECT_EQ(rem[1], 42);

  lhs[1] = 2000;
  // (2000,20005) by 10 -> (200,2000) rem (0,5)
  multiword_unsigned_divide(lhs, 2, rhs, 1, quot, rem);
  EXPECT_EQ(quot[0], 2000);
  EXPECT_EQ(quot[1], 200);
  EXPECT_EQ(rem[0], 5);
  EXPECT_EQ(rem[1], 0);
}

// Generate random test data:
// * Generate 2 numbers, multiplies them and then adds a third
//   number to make it a non-multiple of either.
// * Divide by first number and check results.
// * Divide by second number and check results.
// The third number should be small than the first two.
// Starts with a 2x3 word multiply and increases size on
// each iteration.
TEST(DivideTest, Case3) {
  const int iterations = 100;
  const int size = iterations + 3;
  std::random_device dev;
  std::mt19937 rng(dev());
  std::uniform_int_distribution<unsigned> rand_word(
      1, std::numeric_limits<unsigned>::max());
  uint32_t lhs[size]{rand_word(rng), rand_word(rng)};
  uint32_t rhs[size]{rand_word(rng)};
  uint32_t prod[size * 2];
  uint32_t quot[size * 2];
  uint32_t rem[size * 2];
  uint32_t adj[size * 2]{42};
  std::fill(adj + 1, adj + size, 0);
  for (int iteration{0}; iteration < 100; ++iteration) {
    const int lhs_size = iteration + 3;
    const int rhs_size = iteration + 2;
    const int prod_size = iteration * 2 + 5;
    lhs[iteration + 2] = rand_word(rng);
    rhs[iteration + 1] = rand_word(rng);
    adj[iteration + 1] = adj[iteration];
    adj[iteration] = rand_word(rng);
    // Multiply lhs and rhs.
    multiword_unsigned_multiply(lhs, lhs_size, rhs, rhs_size, prod);
    // Increase by factor smaller than either lhs or rhs (will become
    // remainder).
    uint32_t carry{0};
    for (int w{0}; w != prod_size; ++w) {
      uint32_t part_sum = prod[w] + adj[w];
      if (carry) {
        carry = ++part_sum <= prod[w];
      } else {
        carry = part_sum < prod[w];
      }
      prod[w] = part_sum;
    }
    assert(carry == 0);
    // Divide prod by lhs -> rhs rem adj
    multiword_unsigned_divide(prod, prod_size, lhs, lhs_size, quot, rem);
    for (int w = 0; w < iteration + 2; ++w) {
      EXPECT_EQ(quot[w], rhs[w]) << "quot[" << w << "]";
      EXPECT_EQ(rem[w], adj[w]) << "rem[" << w << "]";
    }
    // Divide prod by rhs -> lhs rem adj
    multiword_unsigned_divide(prod, prod_size, rhs, rhs_size, quot, rem);
    for (int w = 0; w < iteration + 3; ++w) {
      EXPECT_EQ(quot[w], lhs[w]) << "quot[" << w << "]";
      EXPECT_EQ(rem[w], adj[w]) << "rem[" << w << "]";
    }
  }
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
