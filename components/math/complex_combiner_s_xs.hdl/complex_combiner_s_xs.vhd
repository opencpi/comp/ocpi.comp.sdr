-- complex_combiner_s_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of worker is

  constant delay_c : integer := 0;

  signal input_real_take      : std_logic;
  signal input_imaginary_take : std_logic;
  signal combiner_out         : std_logic_vector(output_out.data'length - 1 downto 0);
begin

  input_real_out.take      <= input_real_take;
  input_imaginary_out.take <= input_imaginary_take;

  -- Always take non-sample opcodes from both inputs.
  -- Only take sample opcodes if both ports have a sample opcode.
  input_take_logic_p : process (output_in.ready, input_real_in, input_imaginary_in)
  begin
    -- INPUT 1 (REAL)
    -- If input and output are ready
    input_real_take <= '0';
    if output_in.ready = '1' then
      -- Always take when a non-sample opcode or not valid
      if input_real_in.opcode /= short_timed_sample_sample_op_e or input_real_in.valid = '0' then
        input_real_take <= '1';
      -- Only take a sample opcode if input imaginary is valid,
      -- and also a sample opcode
      elsif input_imaginary_in.valid = '1' and
        input_imaginary_in.opcode = short_timed_sample_sample_op_e then
        input_real_take <= '1';
      end if;
    end if;
    -- INPUT 2 (IMAGINARY)
    -- If input and output are ready
    input_imaginary_take <= '0';
    if output_in.ready = '1' then
      -- Always take when a non-sample opcode or not valid
      if input_imaginary_in.opcode /= short_timed_sample_sample_op_e or input_imaginary_in.valid = '0' then
        input_imaginary_take <= '1';
      -- Only take a sample opcode if input real is valid, and also a
      -- sample opcode.
      elsif input_real_in.valid = '1' and
        input_real_in.opcode = short_timed_sample_sample_op_e then
        input_imaginary_take <= '1';
      end if;
    end if;
  end process;

  -- These handle the translation of non-sample message data from
  -- the narrow input to wider output.
  -- It has zero delay to match the calculation below.
  imaginary_opcode_passthrough_gen : if input_real_opcode_passthrough = '0' generate
    -- Passthrough all non-sample opcodes signals from the imaginary port
    -- Discard all non-sample opcodes from the real port
    imag_interface_delay_i : entity work.imaginary_short_to_complex_short_protocol_delay
      generic map (
        delay_g          => delay_c,
        data_in_width_g  => input_imaginary_in.data'length,
        data_out_width_g => output_out.data'length
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => output_in.ready,
        take_in             => input_imaginary_take,
        input_in            => input_imaginary_in,
        processed_stream_in => combiner_out,
        output_out          => output_out
        );
  end generate;

  real_opcode_passthrough_gen : if input_real_opcode_passthrough = '1' generate
    -- Passthrough all non-sample opcodes signals from the real port
    -- Discard all non-sample opcodes from the imaginary port
    real_interface_delay_i : entity work.real_short_to_complex_short_protocol_delay
      generic map (
        delay_g          => delay_c,
        data_in_width_g  => input_real_in.data'length,
        data_out_width_g => output_out.data'length
        )
      port map (
        clk                 => ctl_in.clk,
        reset               => ctl_in.reset,
        enable              => output_in.ready,
        take_in             => input_real_take,
        input_in            => input_real_in,
        processed_stream_in => combiner_out,
        output_out          => output_out
        );
  end generate;

  -- Performs combination operation
  combiner_out <= (std_logic_vector(input_imaginary_in.data) &
                   std_logic_vector(input_real_in.data));

end rtl;
