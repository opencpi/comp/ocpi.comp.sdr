.. gain_s_l documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: increase magnify


.. _gain_s_l:


Gain (``gain_s_l``)
===================
Multiplies all input sample values by a fixed gain value.

Design
------
The mathematical representation of the implementation is given in :eq:`gain_s_l-equation`.

.. math::
   :label: gain_s_l-equation

   y[n] = G * x[n]

In :eq:`gain_s_l-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`G` is the gain factor.

A block diagram representation of the implementation is given in :numref:`gain_s_l-diagram`.

.. _gain_s_l-diagram:

.. figure:: gain_s_l.svg
   :alt: Block diagram outlining gain implementation.
   :align: center

   Gain implementation.

Interface
---------
.. literalinclude:: ../specs/gain_s_l-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Only values in sample opcode messages are multiplied by a gain value.

As the output data type is larger than the input data type, messages can be split to ensure no output messages are longer than the maximum supported message length of the protocol.

All other opcode messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../gain_s_l.hdl ../gain_s_l.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Protocol interface delay (narrow to wide) primitive v2 <narrow_to_wide_protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``IEEE.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``gain_s_l`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
