#!/usr/bin/env python3

# Python implementation of abs_l for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of abs_l for testing."""

import opencpi.ocpi_testing as ocpi_testing
import numpy as np


class Abs(ocpi_testing.Implementation):
    """Abs test implementation."""

    def __init__(self):
        """Initialise."""
        super().__init__()

    def reset(self):
        """Reset the state to the same state as at initialisation.

        There is no state so this function is a no-op, but it is required by
        the ocpi_testing.Implementation base class.
        """
        pass

    def sample(self, values):
        """Create a set of abs output values.

        Args:
            values (list): Input sample values.

        Returns:
            Formatted messages.
        """
        output_values = [0] * len(values)

        for i, n in enumerate(values):
            if n == -(2**31):
                output_values[i] = 0
            else:
                output_values[i] = abs(n)

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
