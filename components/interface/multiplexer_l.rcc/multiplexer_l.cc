// Multiplexer RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <cstdint>

#include "multiplexer_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Multiplexer_lWorkerTypes;

class Multiplexer_lWorker : public Multiplexer_lWorkerBase {
  RunCondition run_condition;

 public:
  Multiplexer_lWorker()
      : run_condition(1 << MULTIPLEXER_L_INPUT_1 | 1 << MULTIPLEXER_L_OUTPUT,
                      1 << MULTIPLEXER_L_INPUT_2 | 1 << MULTIPLEXER_L_OUTPUT,
                      1 << MULTIPLEXER_L_INSELECT, RCC_NO_PORTS) {
    setRunCondition(&run_condition);
  }

 private:
  bool port_select_enable = false;
  bool port_select = false;

  bool inselect_stored = false;

  RCCResult port_select_enable_written() {
    this->port_select_enable = properties().port_select_enable;
    return RCC_OK;
  }

  RCCResult port_select_written() {
    this->port_select = properties().port_select;
    return RCC_OK;
  }

  RCCResult run(bool) {
    // Update inselect_value to the previously last received value
    bool inselect_value = this->inselect_stored;

    // Use the first inselect sample if available
    if (inselect.isConnected() && inselect.hasBuffer()) {
      int16_t inselect_length = inselect.sample().data().size();
      if (inselect_length &&
          inselect.opCode() == Bool_timed_sampleSample_OPERATION) {
        inselect_value = inselect.sample().data().data()[0];
        // Ensure the last inselect sample is stored for next time
        this->inselect_stored =
            inselect.sample().data().data()[inselect_length - 1];
      }
      inselect.advance();
    }

    // Specify which input to pass through
    bool input_select = this->port_select;
    if (this->port_select_enable) {
      input_select = inselect_value;
    }

    // Pass through input_1 or input_2
    if (!input_select) {
      // Advance non-selected port at the output rate
      if (input_2.hasBuffer() && output.hasBuffer()) {
        input_2.advance();
      }
      // Exit if the selected port or output have no buffer
      if (!input_1.hasBuffer() || !output.hasBuffer()) {
        return RCC_OK;
      }
      // Load output buffer with content of the selected input port
      if (input_1.opCode() == Long_timed_sampleSample_OPERATION) {
        // Pass through sample data
        size_t length = input_1.sample().data().size();
        const int32_t *inData = input_1.sample().data().data();
        int32_t *outData = output.sample().data().data();
        output.setOpCode(Long_timed_sampleSample_OPERATION);
        output.sample().data().resize(length);
        std::copy(inData, inData + length, outData);
      } else if (input_1.opCode() == Long_timed_sampleTime_OPERATION) {
        // Pass through time opcode and data
        output.setOpCode(Long_timed_sampleTime_OPERATION);
        output.time().fraction() = input_1.time().fraction();
        output.time().seconds() = input_1.time().seconds();
      } else if (input_1.opCode() ==
                 Long_timed_sampleSample_interval_OPERATION) {
        // Pass through sample interval opcode and sample interval data
        output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
        output.sample_interval().fraction() =
            input_1.sample_interval().fraction();
        output.sample_interval().seconds() =
            input_1.sample_interval().seconds();
      } else if (input_1.opCode() == Long_timed_sampleFlush_OPERATION) {
        // Pass through flush opcode
        output.setOpCode(Long_timed_sampleFlush_OPERATION);
      } else if (input_1.opCode() == Long_timed_sampleDiscontinuity_OPERATION) {
        // Pass through discontinuity opcode
        output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      } else if (input_1.opCode() == Long_timed_sampleMetadata_OPERATION) {
        // Pass through metadata opcode, id, and data
        output.setOpCode(Long_timed_sampleMetadata_OPERATION);
        output.metadata().id() = input_1.metadata().id();
        output.metadata().value() = input_1.metadata().value();
      } else {
        setError("Unknown OpCode received");
        return RCC_FATAL;
      }
      // Advance selected input
      input_1.advance();
    } else {
      // Advance non-selected port at the output rate
      if (input_1.hasBuffer() && output.hasBuffer()) {
        input_1.advance();
      }
      // Exit if the selected port or output have no buffer
      if (!input_2.hasBuffer() || !output.hasBuffer()) {
        return RCC_OK;
      }
      // Load output buffer with content of the selected input port
      if (input_2.opCode() == Long_timed_sampleSample_OPERATION) {
        // Pass through sample data
        size_t length = input_2.sample().data().size();
        const int32_t *inData = input_2.sample().data().data();
        int32_t *outData = output.sample().data().data();
        output.setOpCode(Long_timed_sampleSample_OPERATION);
        output.sample().data().resize(length);
        std::copy(inData, inData + length, outData);
      } else if (input_2.opCode() == Long_timed_sampleTime_OPERATION) {
        // Pass through time opcode and data
        output.setOpCode(Long_timed_sampleTime_OPERATION);
        output.time().fraction() = input_2.time().fraction();
        output.time().seconds() = input_2.time().seconds();
      } else if (input_2.opCode() ==
                 Long_timed_sampleSample_interval_OPERATION) {
        // Pass through sample interval opcode and sample interval data
        output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
        output.sample_interval().fraction() =
            input_2.sample_interval().fraction();
        output.sample_interval().seconds() =
            input_2.sample_interval().seconds();
      } else if (input_2.opCode() == Long_timed_sampleFlush_OPERATION) {
        // Pass through flush opcode
        output.setOpCode(Long_timed_sampleFlush_OPERATION);
      } else if (input_2.opCode() == Long_timed_sampleDiscontinuity_OPERATION) {
        // Pass through discontinuity opcode
        output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      } else if (input_2.opCode() == Long_timed_sampleMetadata_OPERATION) {
        // Pass through metadata opcode, id, and data
        output.setOpCode(Long_timed_sampleMetadata_OPERATION);
        output.metadata().id() = input_2.metadata().id();
        output.metadata().value() = input_2.metadata().value();
      } else {
        setError("Unknown OpCode received");
        return RCC_FATAL;
      }
      // Advance selected input
      input_2.advance();
    }
    output.advance();
    return RCC_OK;
  }
};

MULTIPLEXER_L_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
MULTIPLEXER_L_END_INFO
