-- HDL Implementation of a Timestamp Inserter.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

--------------------------------------------------------------------------------
-- Timestamp-Inserter
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;
use ocpi.util.all;

library sdr_util;
use sdr_util.sdr_util.opcode_t;
use sdr_util.sdr_util.slv_to_opcode;
use sdr_util.sdr_util.opcode_to_slv;

architecture rtl of worker is

  constant data_width_c                  : integer                                              := input_in.data'length;
  constant timestamp_width_c             : integer                                              := 96;
  constant timestamp_cycles_c            : integer                                              := integer(ceil(real(timestamp_width_c) / real(data_width_c)));
  constant timestamp_cycles_count_bits_c : integer                                              := integer(ceil(log2(real(timestamp_cycles_c))));
  constant opcode_width_c                : integer                                              := input_in.opcode'length;
  constant byte_enable_width_c           : integer                                              := input_in.byte_enable'length;
  constant data_cycle_default_c          : worker_input_in_t                                    := ('0', '0', '0', (others => '0'), (others => '0'), opcode_to_slv(sample_op_e, opcode_width_c), '0', '0', '0', '0');
  constant max_count_c                   : unsigned(timestamp_cycles_count_bits_c - 1 downto 0) := to_unsigned(timestamp_cycles_c - 1, timestamp_cycles_count_bits_c);  -- number of cycles minus one to send timestamp

  type state_t is (idle_s, inmessage_s, inserttimestamp_s);
  signal message_state : state_t;       -- holds state machine state
  signal take          : std_logic;  -- driven when the current input cycle can be taken

  type input_in_skid_t is array (0 to 2) of worker_input_in_t;
  signal input_in_skid : input_in_skid_t;  -- used as a pipeline skid buffer for incoming data

  signal time_now                 : std_logic_vector(63 downto 0);  -- current valid registered time
  -- Holds the timestamp latched at the beginning of insertion. Actual
  -- timestamp is 96 bits vector length chosen to simplify slicing
  -- for all bus bus widths. Top 32 bits will hold padding.
  signal time_now_r               : std_logic_vector(127 downto 0);
  signal timestamp_cycles_count_r : unsigned(timestamp_cycles_count_bits_c - 1 downto 0);  -- timestamp message cycle counter

  signal timestamp_pending : std_logic;  -- capture single timestamp request

begin

  input_out.take <= take;               -- drive input take output

  -- generate the input take signal
  -- it is driven to the output and used to control input to data pipeline
  drive_take_p : process(ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      take <= '0';
      if output_in.ready = '1' and      -- the output is ready
        message_state /= inserttimestamp_s and  -- currently inserting timestamp
        not (input_in_skid(0).ready = '1' and input_in_skid(0).som = '1' and (props_in.continuous_mode = '1' or timestamp_pending = '1') and
             slv_to_opcode(input_in_skid(0).opcode) = sample_op_e) and  -- about to start inserting timestamp
        not (message_state /= idle_s and input_in_skid(2).ready = '1') then  -- idle and pipeline full
        take <= '1';
      end if;
    end if;
  end process;

  -- control flow of incoming data pipeline
  pipeline_shift_p : process(ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then

      -- the pipeline can move along if the output is ready,
      -- a timestamp is not currently being injected
      -- and a new message is not about to start
      if output_in.ready = '1' and
        message_state /= inserttimestamp_s and
        not (message_state = idle_s and input_in_skid(0).ready = '1' and input_in_skid(0).som = '1') then

        input_in_skid        <= input_in_skid(1 to 2) & data_cycle_default_c;  -- default to shifting pipeline when output ready and not inserting timestamp
        -- always push the current value of the incoming eof onto the end of
        -- pipeline to ensure the eof is not output until all pipelined
        -- messages have been sent
        input_in_skid(2).eof <= input_in.eof;

        if take = '1' and (input_in.ready = '1' or input_in.eof = '1') then  -- save valid incoming cycle or eof seen
          -- search for last used pipeline stage and store the incoming data
          -- in that stage as pipeline is being shifted
          if input_in_skid(2).ready = '1' then     -- last stage is used
            input_in_skid(2) <= input_in;
          elsif input_in_skid(1).ready = '1' then  -- second stage is used
            input_in_skid(1) <= input_in;
          else                          -- use first stage
            input_in_skid(0) <= input_in;
          end if;
        end if;

      else                              -- no outgoing
        if take = '1' and input_in.ready = '1' then  -- valid incoming with no outgoing store in available slot
          -- search for first unused pipeline stage and store the incoming data
          -- in that stage as pipeline is not being shifted
          if input_in_skid(0).ready = '0' then       -- first stage empty
            input_in_skid(0) <= input_in;
          elsif input_in_skid(1).ready = '0' then    -- second stage empty
            input_in_skid(1) <= input_in;
          else
            input_in_skid(2) <= input_in;            -- last stage
          end if;
        end if;
      end if;

      if ctl_in.reset = '1' then        -- reset control signals in pipeline
        input_in_skid(0).ready <= '0';
        input_in_skid(0).valid <= '0';
        input_in_skid(0).eof   <= '0';
        input_in_skid(1).ready <= '0';
        input_in_skid(1).valid <= '0';
        input_in_skid(1).eof   <= '0';
        input_in_skid(2).ready <= '0';
        input_in_skid(2).valid <= '0';
        input_in_skid(2).eof   <= '0';
      end if;

    end if;
  end process;

  -- counter used to index through the bytes of an outgoing timestamp message
  insert_timestamp_count_p : process (ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' or message_state = idle_s then  -- set the count to zero when in idle or reset
        timestamp_cycles_count_r <= (others => '0');
      elsif output_in.ready = '1' then
        if message_state = inserttimestamp_s then
          if timestamp_cycles_count_r = max_count_c then
            timestamp_cycles_count_r <= (others => '0');  -- last cycle of timestamp, reset count
          else
            timestamp_cycles_count_r <= timestamp_cycles_count_r + 1;  -- increment timestamp cycle
          end if;
        end if;
      end if;
    end if;
  end process;

  -- drive the output_out signal
  -- output the stored incoming data from the pipeline
  -- or a timestamp message
  drive_output_p : process(ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if output_in.ready = '1' then

        case message_state is
          when idle_s =>                -- output quiet bus when idle
            output_out.eof         <= input_in_skid(0).eof;  -- always pass through eof
            output_out.data        <= (others => '0');
            output_out.byte_enable <= (others => '0');
            output_out.valid       <= '0';
            output_out.som         <= '0';
            output_out.eom         <= '0';
            output_out.opcode      <= opcode_to_slv(sample_op_e, opcode_width_c);
            output_out.give        <= '0';

          when inmessage_s =>  -- drive next message cycle from pipeline
            output_out.data        <= input_in_skid(0).data;
            output_out.byte_enable <= input_in_skid(0).byte_enable;
            output_out.valid       <= input_in_skid(0).valid;
            output_out.som         <= input_in_skid(0).som;
            output_out.eom         <= input_in_skid(0).eom;
            output_out.opcode      <= input_in_skid(0).opcode;
            output_out.give        <= input_in_skid(0).ready;
            output_out.eof         <= '0';

          when inserttimestamp_s =>     -- drive output with cycle of timestamp
            output_out.som         <= '0';        -- default to '0'
            output_out.eom         <= '0';        -- default to '0'
            output_out.valid       <= '1';
            output_out.byte_enable <= (others => '1');
            if data_width_c = 64 and timestamp_cycles_count_r = max_count_c then  -- take care of byte enables when outputting last cycle of TS (32 bits) on 64 bit bus
              output_out.byte_enable(byte_enable_width_c - 1 downto byte_enable_width_c/2) <= (others => '0');
            end if;
            output_out.opcode <= opcode_to_slv(time_op_e, opcode_width_c);
            output_out.give   <= '1';
            output_out.eof    <= '0';
            output_out.data   <= time_now_r(data_width_c - 1 downto 0);  -- output slice of timestamp, vector shifted as consumed
            if timestamp_cycles_count_r = 0 then  -- override som in first cycle
              output_out.som <= '1';
            elsif timestamp_cycles_count_r = max_count_c then  -- override eom in last cycle
              output_out.eom <= '1';
            end if;

          when others =>
            output_out.data        <= (others => '0');
            output_out.byte_enable <= (others => '0');
            output_out.valid       <= '0';
            output_out.som         <= '0';
            output_out.eom         <= '0';
            output_out.opcode      <= opcode_to_slv(sample_op_e, opcode_width_c);
            output_out.eof         <= '0';
            output_out.give        <= '0';
        end case;
      end if;
      -- Reset control signals, excluding data paths. Coded this way to avoid
      -- having to replicate all branches containing data assignments outside
      -- of a traditional "if reset ..., else ..." implementation (just
      -- removing data from the reset branch of such an implementation would
      -- connect the reset to the enable input of the data registers).
      if ctl_in.reset = '1' then        -- reset outputs
        output_out.valid <= '0';
        output_out.give  <= '0';
        output_out.eof   <= '0';
      end if;
    end if;
  end process;

  -- process generates the internal timestamp_pending (single ts insert) value
  -- based on the input elements of props_in
  timestamp_pending_capture_p : process (ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then        -- reset timestamp pending
        timestamp_pending <= '0';
      elsif props_in.continuous_mode = '1' or message_state = inserttimestamp_s then
        -- if in continuous mode or inserting timestamp clear timestamp pending
        timestamp_pending <= '0';
      elsif props_in.timestamp_pending = '1' and props_in.timestamp_pending_written = '1' then
        -- request for single timestamp insertion so set timestamp pending
        timestamp_pending <= '1';
      end if;
    end if;
  end process;

  -- drive the timestamp pending output
  -- '1' when a timestamp insertion request is pending, otherwise '0'
  props_out.timestamp_pending <= timestamp_pending;

  -- process registers current time when valid
  register_current_time_p : process (ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if time_in.valid = '1' then
        time_now <= std_logic_vector(time_in.seconds) & std_logic_vector(time_in.fraction);
      end if;
    end if;
  end process;

  -- State machine indicating progress of a message
  -- three states idle_s, inmessage_s, inserttimestamp_s
  -- idle_s is entered between messages
  --   when in idle_s and the start of a new message is detected it is
  --   determined whether a timestamp needs to be inserted and appropriate
  --   state moved into
  -- inmessage_s indicates a message from the input is being output
  --   when the eom of the current message is detected the state either returns
  --   to idle_s or if a new message is already queued moves to inserttimestamp
  --   or stays in inmessage as approriate
  -- inserttimestamp_s indicated a timestamp message is being inserted
  --   when the last cycle of the timestamp is reached the state moves
  --   to inmessage_s
  state_machine_p : process(ctl_in.clk) is
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then        -- reset state
        message_state <= idle_s;
      else
        case message_state is
          when idle_s =>                -- between messages
            if output_in.ready = '1' then
              if input_in_skid(0).ready = '1' then  -- there is a valid message cycle ready to transmit
                if input_in_skid(0).som = '1' and (props_in.continuous_mode = '1' or timestamp_pending = '1') and
                  slv_to_opcode(input_in_skid(0).opcode) = sample_op_e then  -- start of message that requires a timestamp inserted
                  message_state <= inserttimestamp_s;
                  -- grab the current timestamp
                  time_now_r    <= X"00000000" &  -- top 32 bits are padded to aid slicing for larger bus widths
                                   time_now &
                                   X"00000000";  -- bottom 32 bits are padded as fractional part of time port is 32 bits
                else
                  message_state <= inmessage_s;
                end if;
              end if;
            end if;

          when inmessage_s =>           -- outputting input message
            if output_in.ready = '1' then
              if input_in_skid(0).eom = '1' then  -- if end of message reached check for new message in pipeline
                if input_in_skid(1).ready = '1' and input_in_skid(1).som = '1' then
                  if (props_in.continuous_mode = '1' or timestamp_pending = '1') and
                    slv_to_opcode(input_in_skid(1).opcode) = sample_op_e then  -- start of message that requires a timestamp inserted
                    message_state <= inserttimestamp_s;
                    -- grab the current timestamp
                    time_now_r    <= X"00000000" &  -- top 32 bits are padded to aid slicing for larger bus widths
                                     time_now &
                                     X"00000000";  -- bottom 32 bits are padded as fractional part of time port is 32 bits
                  else  -- no timestamp required go straight to message
                    message_state <= inmessage_s;
                  end if;
                else    -- if no new message in pipeline go to idle
                  message_state <= idle_s;
                end if;
              end if;
            end if;

          when inserttimestamp_s =>     -- outputting timestamp message
            if output_in.ready = '1' then
              if timestamp_cycles_count_r = max_count_c then  -- last timestamp cycle
                message_state <= inmessage_s;
              end if;
              time_now_r <= std_logic_vector(shift_right(unsigned(time_now_r), data_width_c));  -- timestamp shifted each cycle
            end if;

          when others =>                -- between messages
            message_state <= idle_s;

        end case;
      end if;
    end if;
  end process;

end rtl;
