.. message_length_adjuster HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _message_length_adjuster-HDL-worker:


``message_length_adjuster`` HDL Worker
======================================

Detail
------
Unlike the RCC worker, messages are not buffered. Only the message boundaries, SOM and EOM, are altered and back-pressure applied to achieve the desired message sizes at the output.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilization::
