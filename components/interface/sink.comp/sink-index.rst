.. sink documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: sink terminator terminate end discard drop disconnect


.. _sink:


Sink (``sink``)
===============
The sink component allows a non-optional output port of another component to be terminated.

Design
------
The sink component can either take input data whenever it is available, or never take any input data. Any input data that is taken is discarded. The mode of operation is set by the ``take_data`` property. The sink component is typically used for testing purposes.

When ``take_data`` is ``true`` sink will enter the finished control state if it receives an EOF (End-Of-File) message on its input port. When ``take_data`` is ``false`` sink will never enter the finished control state.

Interface
---------
.. literalinclude:: ../specs/sink-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
No protocol is specified for the sink component. It will take data from its input port regardless of the opcode value.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../sink.hdl ../sink.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``sink`` are:

 * Sink has no protocol associated with its input, however its input interface has been configured to be compatible with the ``timed_sample`` collection of protocols. Usage with other protocols may result in undefined behaviour.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
