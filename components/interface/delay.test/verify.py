#!/usr/bin/env python3

# Runs checks for delay testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Delay OCPI Verification script."""

import os
import sys

import opencpi.ocpi_testing as ocpi_testing

from delay import Delay


width_property = int(os.environ["OCPI_TEST_width"])

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

delay_implementation = Delay()

test_id = ocpi_testing.get_test_case()

if width_property == 8:
    protocol = "uchar_timed_sample"
elif width_property == 16:
    protocol = "ushort_timed_sample"
else:
    protocol = "ulong_timed_sample"

verifier = ocpi_testing.Verifier(delay_implementation)
verifier.set_port_types([protocol], [protocol], ["equal"])

if not verifier.verify(test_id, [input_file_path], [output_file_path]):
    sys.exit(1)
