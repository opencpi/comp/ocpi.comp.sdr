.. metadata_inserter documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _metadata_inserter:


Metadata Inserter (``metadata_inserter``)
=========================================
Inserts a metadata message into the stream of data when prompted to do so, via either the trigger port or a property write.

Design
------
Inserts a metadata message, with configurable id and value properties, when the property ``insert_metadata_message`` is set to true or, if the optional ``trigger`` port receives a sample opcode with a boolean value of true.

Data received on the input port is passed through unchanged to the output port. When a sample message with a value of false is received on the optional ``trigger`` port, the behaviour remains unchanged allowing data to pass through unchanged to the output port. When a sample message with a value of true is received on the optional ``trigger`` port, or when the property ``insert_metadata_message`` is set to true, back pressure is applied to the input interface. A metadata message with id/value set to ``metadata_id`` and ``metadata_value`` is output. Input back pressure is then removed and data passes onwards.

If the ``increment_value`` property is set, ``metadata_value`` is incremented after each message insertion. The ``metadata_value`` property will then be updated after incrementing.

If the ``insert_metadata_message`` property is set to true, a single metadata message is inserted. The ``insert_metadata_message`` property is cleared to false by the component after the insertion of a metadata message.

If the ``trigger`` port is connected, it discards all non-sample opcodes received on that port.  Samples within sample opcode messages are consumed at the same rate as the ``input`` port consumes samples when the ``input`` port opcode is set to samples. Message boundaries are only observed from the ``input`` port, while message boundaries on the ``trigger`` port are ignored.

If the ``trigger`` port is not connected or the ``trigger`` port indicates the EOF flag is set then any sample opcodes on the ``input`` port are sent directly to the ``output`` port.

Data Flow
~~~~~~~~~
The rate at which the trigger and input ports are consumed is dependent on the opcode associated with the port:

* Trigger and input port are sample opcodes: Both ports consumed at the same rate.

* Trigger port is sample opcode; input port is non-sample opcode: Trigger port blocked; input port consumed.

* Trigger port is non-sample opcode; input port is sample opcode: Trigger port consumed; input port blocked.

* Trigger port is non-sample opcode; input port is non-sample opcode: Both ports consumed at the same rate.

Interface
---------
.. literalinclude:: ../specs/metadata_inserter-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~

All messages on the input port are passed through to the output port, regardless of opcode.

A sample opcode received on the ``trigger`` port with a value of ``True`` will trigger the insertion of a metadata message, all other opcodes on the trigger port are discarded. This insertion will occur such that the matched sample on the input port will be the first sample in the sample message following the inserted metadata message.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   insert_metadata_message: Resets to false after insertion is completed.

Ports
~~~~~
.. ocpi_documentation_ports::

Implementations
---------------
.. ocpi_documentation_implementations:: ../metadata_inserter.hdl ../metadata_inserter.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Metadata inserter primitive v2 <metadata_inserter_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

Limitations
-----------
Limitations of ``metadata_inserter`` are:

 * Testing of triggers through setting the property ``insert_metadata_message`` is not fully tested: It is only set before the input port is ready, so is only tested as a delayed trigger and not on an active input port stream (whether directly during a sample opcode or by a delayed trigger during a non-sample opcode).

 * Triggering multiple metadata messages through ``insert_metadata_message`` when the output is providing back pressure will result in only a single metadata message being inserted.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
