// Timestamp Inserter RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <chrono>

#include "../../util/common/opcodes.hh"
#include "timestamp_inserter-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Timestamp_inserterWorkerTypes;

class Timestamp_inserterWorker : public Timestamp_inserterWorkerBase {
  RCCBoolean continuous_mode = false;
  RCCBoolean timestamp_pending = false;
  RCCBoolean timestamp_sent = false;
  std::chrono::seconds leap_seconds{0};
  uint32_t gps_time_seconds{0};
  uint64_t gps_time_fraction{0};

#if __cplusplus < 202002L
  // gps time differs from UTC (system time) by:
  // epoch 00:00:00 6 January 1980 (3657 days = (10x365 day years) + (2
  // leap days) + (1st->6th Jan)):
  const std::chrono::seconds gps_epoch =
      std::chrono::seconds(3657L * 24L * 60L * 60L);
#endif

  // The chrono library gives time in standard (base 10) subdivisions of
  // seconds,
  // the smallest being nanoseconds. This needs to be converted to Q32.64.
  // The nanosecond to 64bit binary conversion is 2^64 / 10^9 = 18446744073.
  const uint64_t nano_to_64bit = 18446744073u;

  RCCResult continuous_mode_written() {
    this->continuous_mode = properties().continuous_mode;
    return RCC_OK;
  }

  RCCResult timestamp_pending_written() {
    this->timestamp_pending = properties().timestamp_pending;
    return RCC_OK;
  }

  RCCResult leap_seconds_written() {
    // Set the leap seconds value.
    this->leap_seconds = std::chrono::seconds(properties().leap_seconds);
    return RCC_OK;
  }

  // Update current_time from system clock.
  void update_current_time() {
#if __cplusplus >= 202002L
    auto time_since_epoch = std::chrono::gps_clock::now().time_since_epoch();
#else
    auto time_since_epoch =
        std::chrono::high_resolution_clock::now().time_since_epoch() -
        this->leap_seconds - this->gps_epoch;
#endif
    this->gps_time_seconds =
        std::chrono::duration_cast<std::chrono::seconds>(time_since_epoch)
            .count();
    time_since_epoch -= std::chrono::seconds(this->gps_time_seconds);
    this->gps_time_fraction =
        std::chrono::duration_cast<std::chrono::nanoseconds>(time_since_epoch)
            .count() *
        nano_to_64bit;
  }

  RCCResult run(bool) {
    if (input.opCode() == Timed_sampleSample_OPERATION) {
      if ((this->continuous_mode || this->timestamp_pending) &&
          !this->timestamp_sent) {
        // Insert timestamp
        update_current_time();
        output.setInfo(
            Timed_sampleTime_OPERATION,
            sizeof(this->gps_time_fraction) + sizeof(this->gps_time_seconds));
        uint8_t* input_data =
            reinterpret_cast<uint8_t*>(&this->gps_time_fraction);
        uint8_t* output_data = output.data();
        std::copy(input_data, input_data + sizeof(this->gps_time_fraction),
                  output_data);
        output_data += sizeof(this->gps_time_fraction);

        input_data = reinterpret_cast<uint8_t*>(&this->gps_time_seconds);
        std::copy(input_data, input_data + sizeof(this->gps_time_seconds),
                  output_data);
        output.advance();

        // Clear local and external timestamp_pending flags
        this->timestamp_pending = false;
        properties().timestamp_pending = false;
        this->timestamp_sent = true;
        return RCC_OK;
      }

      this->timestamp_sent = false;
      return forward_opcode(input, output);
    }

    // Pass through all other opcodes and data
    return forward_opcode(input, output);
  }
};

TIMESTAMP_INSERTER_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
// YOU MUST LEAVE THE *START_INFO and *END_INFO macros here and uncommented in
// any case
TIMESTAMP_INSERTER_END_INFO
