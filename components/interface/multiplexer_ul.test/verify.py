#!/usr/bin/env python3

# Runs checks for multiplexer testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verify the output of the multiplexer."""

import os
import sys

import opencpi.ocpi_testing as ocpi_testing

from multiplexer import Multiplexer

port_select_property = os.environ["OCPI_TEST_port_select"].upper() == "TRUE"

input_1_file = str(sys.argv[-3])
input_2_file = str(sys.argv[-2])
output_file = str(sys.argv[-4])

multiplexer_implementation = Multiplexer(port_select_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(multiplexer_implementation)
verifier.set_port_types(["ulong_timed_sample", "ulong_timed_sample"],
                        ["ulong_timed_sample"],
                        ["equal"])

if verifier.verify(
        test_id, [input_1_file, input_2_file], [output_file]) is True:
    sys.exit(0)
else:
    sys.exit(1)
