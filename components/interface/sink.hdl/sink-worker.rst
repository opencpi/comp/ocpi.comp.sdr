.. sink HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _sink-HDL-worker:


``sink`` HDL Worker
===================

Detail
------
To avoid back-pressure when using the HDL implementation, the width of the sink component's input port should match the width of the output port it is connecting to. For example, to terminate an output port using the ``bool_timed_sample`` protocol, the ``sink.hdl`` worker should be used with the ``bit_width`` parameter set to ``8``, while to terminate an output port using the ``complex_short_timed_sample`` protocol, the worker should be used with the ``bit_width`` parameter set to ``32``.

.. ocpi_documentation_worker::

   bit_width: Defines the width of the Stream interface of this worker. Should be a multiple of 8.

Utilisation
-----------
.. ocpi_documentation_utilization::
