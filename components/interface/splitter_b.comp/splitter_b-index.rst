.. splitter_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _splitter_b:


Splitter (``splitter_b``)
=========================
Splits a single input into multiple outputs.

Design
------
This Splitter implementation splits (purely duplicates) the input to one or more (up to four) outputs. All connected outputs must be ready to accept data before any data is output by the component.
The RCC worker checks that all connected ports are ready before outputting data.
The HDL worker uses the ``output_X_in.reset`` signal to test whether an output port is connected. If a port is not connected the logic by default sets it to be ready all the time.

The mathematical representation of the implementation is given in :eq:`splitter_b-equation`.

.. math::
   :label: splitter_b-equation

   y_1[n] = x[n]\\ y_2[n] = x[n]\\ y_3[n] = x[n]\\ y_4[n] = x[n]\\

In :eq:`splitter_b-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y_1[n]` is the output values.

 * :math:`y_2[n]`, :math:'y_3[n]' and :math:'y_4[n]' are the optional output values.

A block diagram representation of the implementation is given in :numref:`splitter_b-diagram`.

.. _splitter_b-diagram:

.. figure:: splitter_b.svg
   :alt: Splitter diagram
   :align: center

   Splitter diagram

Interface
---------
.. literalinclude:: ../specs/splitter_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Input samples port.
   output_1: Output samples port.
   output_2: Optional output samples port.
   output_3: Optional output samples port.
   output_4: Optional output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
All Opcodes are passed through, duplicated to each output port.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../splitter_b.hdl ../splitter_b.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
