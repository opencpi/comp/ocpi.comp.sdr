<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
  </case>
  <!-- Note, testing with low values for step_size will take 100s of hours
       to complete. Limiting to larger values for speed. -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size"
      values="8346192,4294967294,4294967295,1808245350,2134556258,3437443452,1232976562,3725441660,2056267562"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="step_size"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" values="true,false"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="enable"/>
  </case>
  <!-- Have no way to know when worker was lacking input data when testing. So
       cannot verify testing when zero_pad_when_not_ready is zero, therefore
       not including test. -->
  <!--
  <case>
    <input port="input" script="generate.py -case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" values="true,false"/>
    <property name="subcase" value="zero_pad_when_not_ready"/>
  </case>
  -->
  <!-- Cannot currently verify as no way to determine when property changed in
       relation to data. -->
  <!--
  <case>
    <input port="input" script="generate.py -c property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size">
      <set delay="0" value="160049103"/>
      <set delay="1" value="2147483548"/>
    </property>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="step_size"/>
  </case>
  <case>
    <input port="input" script="generate.py -c property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size"/>
    <property name="enable">
      <set delay="0" value="false"/>
      <set delay="1" value="true"/>
    </property>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" value="zero_pad_when_not_ready"/>
  </case>
  <case>
    <input port="input" script="generate.py -c property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready">
      <set delay="0" value="false"/>
      <set delay="1" value="true"/>
    </property>
    <property name="subcase" value="zero_pad_when_not_ready"/>
  </case>
  -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase"
      values="all_zero,all_maximum,all_minimum,large_positive,large_negative,near_zero,real_zero,imaginary_zero"/>
  </case>
  <!-- Test never terminates when attribute stressormode is set to full since
       a ZLM is not sent at the end in OpenCPI 1.4. -->
  <!--
  <case>
    <input port="input" script="generate.py -case input_stressing" messagesinfile="true" messagesize="16384"
      stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
  </case>
  -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="42949672"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size" value="160049103"/>
    <property name="enable" value="true"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Note, testing with low values for step_size will take 100s of hours
       to complete. Limiting to larger values for speed. -->
  <!-- Cannot test with zero_pad_when_not_ready is true, as have no way of
       knowing when a worker was lacking input data when testing. So cannot
       verify testing, therefore not including test.-->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="step_size"
      values="1129006152,1669928031,4064525061,3461556096,4211018350,4180444758,1592769263,1912931052,1216733261,1872655713"/>
    <property name="enable" values="true,false"/>
    <property name="zero_pad_when_not_ready" value="false"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
