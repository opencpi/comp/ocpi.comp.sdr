#!/usr/bin/env python3

# Generates the input binary file for multiplexer testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generate input binary files for multiplexer testing."""

import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
port_select_enable = os.environ.get("OCPI_TEST_port_select_enable") == "TRUE"
port_select = os.environ.get("OCPI_TEST_port_select") == "TRUE"

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  port_select_enable, port_select)

generator = ocpi_testing.generator.LongGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(
        arguments.save_path, "long_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
