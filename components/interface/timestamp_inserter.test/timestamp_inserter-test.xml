<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <!-- Case 00 Typical -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="false"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
  </case>
  <!-- Case 01 Continuous Mode property -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
  </case>
  <!-- Case 02 Timestamp Pending property (continous mode must be set to false for functionality testing)-->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="false"/>
    <property name="timestamp_pending" value="true"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
  </case>
  <!-- Case 03 Property leap_seconds -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="false"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" values="0,19"/>
    <property name="port_width" value="16"/>
  </case>
  <!-- Case 04 Port Width property -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" values="8,16,32,64"/>
  </case>
  <!-- Case 05: Sample values -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="all_zero,all_maximum,large_positive,near_zero"/>
  </case>
  <!-- Case 06: Input stressing -->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
  </case>
  <!-- Case 07: Message size -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 08: Time opcode -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 09: Sample interval opcode -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 10: Flush opcode -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 11: Discontinuity opcode -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 12: Metadata opcode -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" value="true"/>
    <property name="timestamp_pending" value="false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" value="16"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 13 Custom -->
  <case>
    <input port="input" script="generate.py --case custom" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" values="true,false"/>
    <property name="timestamp_pending" value="true"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" values="8,64"/>
    <property name="subcase" values="time,sample_interval,flush,discontinuity,metadata"/>
  </case>
  <!-- Case 14 Soak -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="continuous_mode" values="true,false"/>
    <property name="timestamp_pending" values="true,false"/>
    <property name="leap_seconds" value="18"/>
    <property name="port_width" values="8,16,32,64"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
