-- Golay Encoder
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-------------------------------------------------------------------------------
-- Golay Encoder
-------------------------------------------------------------------------------
--
-- Description:
--
-- Implementation of a Golay Encoder with characteristic polynomial:
-- x^11 + x^9 + x^7 + x^6 + x^5 + x + 1 (0xAE3)
--
-- An overall parity bit is added to extend the codewords to be Golay [24,12,8]
--
-- This implementation uses a ROM of precomputed Golay codewords
-- with input port data used as the address to the ROM.

-- It can only process 12 bit data at a time since it encodes 12 bit data, so
-- it only looks at the 12 LSB bits of the input data port.
--
-- The codewords produced are 24 bits wide, placed in the 24 LSB bits of the
-- output data port.
--
-- Latency:
-- The output has a latency of one output_in.clk clock cycle
--
-- References:
-- http://aqdi.com/articles/using-the-golay-error-detection-and-correction-code-3/
-- https://www.researchgate.net/profile/Satyabrata-Sarangi/publication/264977426_Efficient_Hardware_Implementation_of_Encoder_and_Decoder_for_Golay_Code/links/5405fdd00cf23d9765a7cbca/Efficient-Hardware-Implementation-of-Encoder-and-Decoder-for-Golay-Code.pdf
-- https://www.sciencedirect.com/science/article/pii/S1665642313715438
-- https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.323.9943&rep=rep1&type=pdf
-- http://www.rajivchakravorty.com/source-code/uncertainty/multimedia-sim/html/golay_8c-source.html

-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length
-- LINT EXCEPTION: vhdl_003: -6: URL cannot be shortened to fit line length

-- TODO: Add marshalling and demarshalling primitves to handle the protocols
-- used for the input and output ports.
-- Some of this code will have to be rewritten when the marshalling primitives
-- are added. Note that only the data associated with the sample opcode is
-- encoded by this worker. All other opcodes pass through this component
-- without any effect (with 1 clk cycle delay as well).

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions

architecture rtl of worker is

  signal checkbits_mask : std_logic_vector(15 downto 0);
  signal codeword       : std_logic_vector(23 downto 0);
  signal last_data_r    : std_logic;
  signal take           : std_logic;
  signal give           : std_logic;
  signal first_data_r   : std_logic;
  signal opcode_r       : ushort_timed_sample_opcode_t;
  signal output_opcode  : ulong_timed_sample_opcode_t;
  signal eof_r          : std_logic;
  signal ivld           : std_logic;
  signal valid_r        : std_logic;
  signal som_r          : std_logic;
  signal eom_r          : std_logic;
  signal samples_vld    : std_logic;
  signal output_data    : std_logic_vector(output_out.data'range);
  signal input_data_r   : std_logic_vector(input_in.data'range);

begin

  golay_codewords_inst : entity work.golay_codewords_rom
    port map (
      clk      => output_in.clk,
      ivld     => ivld,
      din      => input_in.data(11 downto 0),
      codeword => codeword);

  checkbits_mask <= from_ushort(props_in.checkbits_mask);

  -- Remove the below line when the marshalling and demarshalling
  -- primitves are added
  samples_vld <= input_in.valid when (input_in.opcode = ushort_timed_sample_sample_op_e) else '0';
  ivld        <= output_in.ready and samples_vld;
  take        <= input_in.ready and output_in.ready;
  give        <= (input_in.ready or last_data_r) and first_data_r and output_in.ready;

  -- xor the checkbits with the checkbit mask to toggle the bits. This is done
  -- to alleviate the situation where there is a hardware failure, so that
  -- hardware that is failing is not sending valid golay codewords.
  output_data <= x"00" & codeword(23) & (codeword(22 downto 12) xor checkbits_mask(10 downto 0)) & codeword(11 downto 0)
                 when (opcode_r = ushort_timed_sample_sample_op_e) else x"0000" & input_data_r;

  output_out.data  <= output_data;
  input_out.take   <= take;
  output_out.give  <= give;
  output_out.valid <= give and valid_r;
  output_out.som   <= give and som_r;
  output_out.eom   <= give and eom_r;
  output_out.eof   <= eof_r;

  output_opcode <= ulong_timed_sample_sample_op_e when (opcode_r = ushort_timed_sample_sample_op_e) else
                   ulong_timed_sample_time_op_e            when (opcode_r = ushort_timed_sample_time_op_e) else
                   ulong_timed_sample_sample_interval_op_e when (opcode_r = ushort_timed_sample_sample_interval_op_e) else
                   ulong_timed_sample_flush_op_e           when (opcode_r = ushort_timed_sample_flush_op_e) else
                   ulong_timed_sample_discontinuity_op_e   when (opcode_r = ushort_timed_sample_discontinuity_op_e) else
                   ulong_timed_sample_metadata_op_e        when (opcode_r = ushort_timed_sample_metadata_op_e) else
                   ulong_timed_sample_sample_op_e;

  output_out.opcode <= output_opcode;

  -- Since it takes a clock cycle for output to change have
  -- to delay these by one clock cycle
  delay_regs : process (output_in.clk)
  begin
    if rising_edge(output_in.clk) then
      if output_in.reset = '1' then
        first_data_r <= '0';
        last_data_r  <= '0';
        input_data_r <= (others => '0');
        opcode_r     <= ushort_timed_sample_sample_op_e;
        valid_r      <= '0';
        som_r        <= '0';
        eom_r        <= '0';
        eof_r        <= '0';
      else
        if (take = '1') then
          valid_r      <= input_in.valid;
          input_data_r <= input_in.data;
          som_r        <= input_in.som;
          eom_r        <= input_in.eom;
          opcode_r     <= input_in.opcode;
        end if;
        -- To handle initial output from golay_encoder_rom when input_in.ready
        -- goes high for the first time
        if (first_data_r = '0' and input_in.ready = '1') then
          first_data_r <= '1';
        end if;
        -- To handle when input_in.ready goes low after an eof arrives
        if (input_in.eof = '1' and eof_r = '0' and output_in.ready = '1') then
          last_data_r <= '1';
        end if;
        if last_data_r = '1' then
          last_data_r <= '0';
          eof_r       <= input_in.eof;
        end if;
      end if;
    end if;
  end process delay_regs;

end rtl;
