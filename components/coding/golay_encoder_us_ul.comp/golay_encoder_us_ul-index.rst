.. golay_encoder_us_ul documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _golay_encoder_us_ul:


Golay Encoder (``golay_encoder_us_ul``)
=======================================
Takes 12 bit input data and encodes it into a Golay codeword.

Tested platforms include ``xsim``, ``e31x``.

Design
------
A Golay code is an error correcting code used for forward error correction in digital communications.

Given a unsigned short, the Golay encoder component looks at the 12 LSB bits and encodes it into a Golay codeword, placed in the LSB bits,
using a Golay encoding algorithm (24 bit wide codeword if the extended Golay code is implemented or 23 bit wide codeword if the perfect
Golay code is implemented).

A block diagram representation of Golay [24,12,8] is given in :numref:`golay_encoder-diagram`.

.. _golay_encoder-diagram:

.. figure:: golay_encoder.svg
   :alt: Block diagram outlining Golay [24,12,8].
   :align: center

   Block diagram outlining Golay [24,12,8].

Interface
---------
.. literalinclude:: ../specs/golay_encoder_us_ul-spec.xml
   :language: xml

Ports
~~~~~
.. ocpi_documentation_ports::

   input:  Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
The Golay encoder encodes data with sample opcodes only.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../golay_encoder_us_ul.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml

Dependencies
------------
The dependency (generated via ``gen_golay_codewords_rom.py``) for this worker is:

 * ``golay_codewords_rom.vhd``

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ocpi.types.all``

Limitations
-----------
Limitations of ``golay_encoder_us_ul`` are:

 * None.

Testing
-------
This component test suite uses the OpenCPI unit test framework. The unit test sends all possible
12 bit sample data and checks that output data is the expected Golay codeword.

.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
