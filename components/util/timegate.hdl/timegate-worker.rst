.. timegate HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _timegate-HDL-worker:


``timegate`` HDL Worker
=======================
The HDL worker input and output ports have data widths matching the underlying protocol to provide throughput up to 1 sample per cycle.

Detail
------

.. ocpi_documentation_worker::

   leap_seconds: This property is ignored by this worker since the time service provides GPS time.


Clock Domains
^^^^^^^^^^^^^
The HDL worker has three clock domains: input (``input_in.clk``), output (``output_in.clk``) and control (``ctl_in.clk``).

Synchronise to Control domain:

The output processing determines whether the stored time has passed before the gate is opened and strobes a signal. If this occurs, the cdc_fast_pulse_to_slow_sticky primitive is used to store the occurrence and pass it to the control clock domain, which is then cleared when the ``late_time_sticky`` property is written to.

The delta between the actual time transmitted to the requested timestamp is passed to the control clock domain using the cdc_bits_feedback primitive.

Synchronise to Output domain:

The input control signals are used to enable the input data, opcode, byte_enable, valid, SOM, EOM and EOF into one side of a clock domain crossing FIFO on the input clock domain. The stored information is extracted from the FIFO and processed on the output clock domain utilising the time interface, which is also on the output clock domain.

The ``bypass`` property is passed to the output clock domain using the cdc_single_bit primitive.

The ``time_correction`` property is passed to the output clock domain using the cdc_bits_feedback primitive.

Constraints
^^^^^^^^^^^

This worker passes signals between different clock domains through primitives designed for that purpose. The primitives may contain attributes and should be paired with constraints to control their placement, optimising the MTBF through the synchronisers, and preventing the timing analyser from needlessly attempting to time the crossing signals. The Xilinx Vivado tool has the capability to report on the quality of the clock domain crossings in the design and can accept waivers for warnings generated for CDC architectures that it cannot automatically verify. Platform/application developers should therefore consider adding the constraints, or similar if not using Vivado, to designs that instantiate the component. Refer to the documentation for all clock domain clocking primitives identified in the dependencies section, and all of their dependencies, for the applicable constraints.

Note, it is perfectly acceptable to connect all three clocks in the Timegate HDL worker to the same source clock if domain crossing is not required. If this is the case, then the component would consume only a small amount of unnecessary logic for the CDC primitives and the constraints would not be required.

Limitations
-----------

 * If the ``time_correction`` property is written within 4 clock cycles of a previous write, the second write will be ignored.

 * If the ``actual_time_to_requested_time_delta`` property attempts to update within 4 clock cycles of a previous update, the second update will be ignored.

Utilisation
-----------
.. ocpi_documentation_utilization::
