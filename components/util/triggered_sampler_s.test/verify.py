#!/usr/bin/env python3

# Runs checks for triggered_sampler_s testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI verification script for triggered_sampler_s tests."""

import os
import pathlib
import sys
import xml.etree.ElementTree as ET
import opencpi.ocpi_testing as ocpi_testing

from triggered_sampler import TriggeredSampler

bypass_property = (os.environ.get("OCPI_TEST_bypass").lower() == "true")
capture_length_property = int(os.environ.get("OCPI_TEST_capture_length"))
send_flush_property = \
    (os.environ.get("OCPI_TEST_send_flush").lower() == "true")
capture_on_meta_property = \
    (os.environ.get("OCPI_TEST_capture_on_meta").lower() == "true")
capture_continuous_property = \
    (os.environ.get("OCPI_TEST_capture_continuous").lower() == "true")

input_input_file = str(sys.argv[-2])
input_trigger_file = str(sys.argv[-1])
output_file = str(sys.argv[-3])

test_id = ocpi_testing.get_test_case()

# Read capture_single property direct from generated test application xml
# since it is a volatile property whose initial value is not set correctly
# as an environment variable
app_file = os.path.join(os.path.dirname(os.path.dirname(input_input_file)),
                        "applications",
                        test_id + ".xml")

case_worker_port = pathlib.Path(output_file).stem.split(".")
component = f"{case_worker_port[-3]}"

capture_single = ET.parse(app_file).findall(
    f"instance[@name='{component}']/property[@name='capture_single']")

capture_single_property = (capture_single[0].attrib["value"] == "true")

triggered_sampler_implementation = TriggeredSampler(
    bypass_property,
    capture_length_property,
    send_flush_property,
    capture_on_meta_property,
    capture_single_property,
    capture_continuous_property)

verifier = ocpi_testing.Verifier(triggered_sampler_implementation)
verifier.set_port_types(
    ["short_timed_sample", "short_timed_sample"],
    ["short_timed_sample"],
    ["equal"])
if verifier.verify(test_id,
                   [input_input_file, input_trigger_file],
                   [output_file]) is True:
    sys.exit(0)
else:
    sys.exit(1)
