#!/usr/bin/env python3

# This file is used to generate taps for the unit tests.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generator for the Tap Property of the Polyphase Interpolator."""

import os
import sys
import scipy.signal as sig
import numpy as np
import opencpi.ocpi_testing as ocpi_testing

max_taps = int(os.environ.get("OCPI_TEST_max_number_taps", 255))
number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
if number_of_taps > max_taps:
    number_of_taps = max_taps

max_interpolation_factor = int(
    os.environ.get("OCPI_TEST_max_interpolation", 255))
interpolation_factor = int(os.environ["OCPI_TEST_interpolation_factor"])
if interpolation_factor > max_interpolation_factor:
    interpolation_factor = max_interpolation_factor

taps_shape = os.environ["OCPI_TEST_taps_shape"]

# Parse arguments
taps_type = sys.argv[1]
taps_fractional_bits = int(sys.argv[2])
output_path = sys.argv[3]

seed = ocpi_testing.get_test_seed(interpolation_factor,
                                  number_of_taps,
                                  max_interpolation_factor,
                                  max_taps,
                                  taps_shape,
                                  taps_type, taps_fractional_bits)

if taps_shape == "kaiser":
    if interpolation_factor > 1:
        taps = (interpolation_factor * sig.firwin(
            number_of_taps,
            1/2,
            window=("kaiser", 5.0)))
    else:
        taps = (interpolation_factor * sig.firwin(
            number_of_taps,
            1/(interpolation_factor),
            window=("kaiser", 5.0)))
elif taps_shape == "all_ones":
    taps = np.ones(number_of_taps)
elif taps_shape == "random":
    np.random.seed(seed)
    # Generate taps in the open range (-1,+1)
    # N.B. Random returns [min,max) so will need to ensure bounds
    max_val = +1
    min_val = -1
    # Random returns values up to, but not including, max_val, but
    # does return down to *and* including min_val.
    # Need to not have exactly -UINT_MAX
    min_val += sys.float_info.epsilon
    taps = np.random.uniform(min_val, max_val,
                             size=(number_of_taps))
else:
    assert False, f"Unrecognised taps_shape: {taps_shape}"

# Scale to integer format
max_abs_taps = max(np.absolute(taps))
integer_taps = [int((x/max_abs_taps)*(pow(2, taps_fractional_bits)-1))
                for x in taps]

assert (np.all(np.array(integer_taps) < 2**taps_fractional_bits)
        and np.all(np.array(integer_taps) >= -2**taps_fractional_bits)), (
    f"Filter taps goes beyond the range allowed for {taps_type} "
    f"({-(2**taps_fractional_bits)} to {(2**taps_fractional_bits)-1})"
    f"\nTaps: {integer_taps}")

with open(output_path, "w") as f:
    for tap in integer_taps:
        f.write(f"{tap}\n")
