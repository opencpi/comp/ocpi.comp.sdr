#!/usr/bin/env python3

# Generate input for the half_band_decimator_xs for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Half Band Decimator test data generator."""

import os
import random
import logging

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing
from ocpi_block_testing.generator.base_block_generator import BaseBlockGenerator
from ocpi_block_testing.generator.complex_short_block_generator import ComplexShortBlockGenerator
from ocpi_block_testing.get_generate_arguments import get_generate_arguments


arguments = get_generate_arguments(generator=BaseBlockGenerator)

subcase = os.environ["OCPI_TEST_subcase"]
rounding_type = os.environ.get("OCPI_TEST_rounding_type")
number_of_taps = int(os.environ["OCPI_TEST_number_of_taps"])
taps_shape = os.environ["OCPI_TEST_taps_shape"]
taps_symmetric = os.environ["OCPI_TEST_taps_symmetric"]

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, rounding_type, number_of_taps, taps_shape, taps_symmetric)


class HalfBandDecimatorGenerator(ComplexShortBlockGenerator):
    """Half Band Decimator test case generator."""

    def __init__(self, **kwds):
        """Initialise."""
        super().__init__(block_length=2)

        # Decimation will double the interval from input to output.
        self.SAMPLE_INTERVAL_MAX = self.SAMPLE_INTERVAL_MAX / 2

    def property(self, seed, subcase):
        """Override of property case for group_delay.

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        random.seed(seed)

        if subcase == "group_delay":
            # Ensure time and interval are included.
            messages = [{"opcode": "sample_interval", "data": 1.0},
                        {"opcode": "time", "data": 10000.0},
                        {"opcode": "sample",
                            "data": self._get_sample_values(1)},
                        {"opcode": "time", "data": 20000.0},
                        {"opcode": "sample",
                            "data": self._get_sample_values(1)},
                        {"opcode": "sample",
                            "data": self._get_sample_values(1)},
                        {"opcode": "sample",
                            "data": self._get_sample_values(1)},
                        {"opcode": "sample",
                            "data": self._get_sample_values(12)},
                        {"opcode": "time", "data": 30000.0}
                        ]
            return messages

        return super().property(seed, subcase)

    def flush(self, seed, subcase):
        """Override of flush cases for initial flush and for odd sample counts.

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        if subcase == "initial":
            # Insert an initial flush opcode before data
            messages = [{"opcode": "flush", "data": None},
                        {"opcode": "sample", "data": self._get_sample_values()}]
            return messages

        if subcase[0:4] == "odd_":
            self.SAMPLE_DATA_LENGTH -= 1
            subcase = subcase[4:]

        return super().flush(seed, subcase)

    def sample_interval(self, seed, subcase):
        """Override of sample_interval to add wrap test.

        Args:
            seed (int): Random number seed.
            subcase (string): Subcase name.

        Returns:
            List of messages.
        """
        if subcase == "wrap":
            # Ensure time and interval wrapping is covered.
            # group_delay should be zero for this test
            messages = [{"opcode": "sample_interval", "data": 1.0},
                        {"opcode": "sample",
                            "data": self._get_sample_values(1)},
                        # Odd sample, so time is incremented which will wrap.
                        {"opcode": "time", "data": self.TIME_MAX},
                        {"opcode": "sample_interval",
                            "data": self.SAMPLE_INTERVAL_MAX},
                        {"opcode": "sample",
                            "data": self._get_sample_values(2)},
                        # Odd sample, so time+interval will wrap.
                        {"opcode": "time", "data": 1.0},
                        {"opcode": "sample", "data": self._get_sample_values(1)}]
            return messages

        return super().sample_interval(seed, subcase)

    def custom(self, seed, subcase):
        """Custom test case.

        Tests non-sample opcodes received when a sample has been decimated away.
        If a sample opcode with a single sample is decimated, there will be no
        sample opcode output. An odd number will decimate a sample but still
        produce a sample opcode output.
        Non-resetting opcodes (time,sample_interval, metadata) require a single
        sample align back on decimation boundary.
        Resetting opcodes (flush,discontinuity) clear the internal buffer so no
        aligning sample is needed.

        Args:
            seed (int): Seed for random generator.
            subcase (string): Subcase name

        Returns:
            Array of messages.
        """
        random.seed(seed)
        if subcase == "single":
            samples_size = 1
        elif subcase == "odd":
            samples_size = random.randint(2, self.SAMPLE_DATA_LENGTH/2)*2-1
        else:
            ValueError(f"Unexpected subcase of {subcase} for custom()")

        messages = [{"opcode": "sample", "data": self._get_sample_values(samples_size)},
                    {"opcode": "time", "data": self.TIME_MAX},
                    # Align back to decimation boundary.
                    {"opcode": "sample", "data": self._get_sample_values(1)},
                    {"opcode": "sample",
                        "data": self._get_sample_values(samples_size)},
                    {"opcode": "sample_interval", "data": 1},
                    # Align back to decimation boundary.
                    {"opcode": "sample", "data": self._get_sample_values(1)},
                    {"opcode": "sample",
                        "data": self._get_sample_values(samples_size)},
                    {"opcode": "metadata", "data": {"id": 0, "value": 0}},
                    # Align back to decimation boundary.
                    {"opcode": "sample", "data": self._get_sample_values(1)},
                    {"opcode": "sample",
                        "data": self._get_sample_values(samples_size)},
                    {"opcode": "flush", "data": None},
                    # Internal buffers reset
                    {"opcode": "sample",
                        "data": self._get_sample_values(samples_size)},
                    {"opcode": "discontinuity", "data": None},
                    # Internal buffers reset
                    {"opcode": "sample", "data": self._get_sample_values(samples_size)}]
        # Odd sample at EOF

        return messages


log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 7:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

generator = HalfBandDecimatorGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
