#!/usr/bin/env python3

# Generates input binary file for Fast Fourier Transform XS testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""OpenCPI generator for fast_fourier_transform_xs tests."""

import os
import random
import numpy as np
import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing
from ocpi_block_testing.generator.base_block_generator import BaseBlockGenerator
from ocpi_block_testing.generator.complex_short_block_generator import ComplexShortBlockGenerator
from ocpi_block_testing.get_generate_arguments import get_generate_arguments


class FastFourierTransformGenerator(ComplexShortBlockGenerator):
    """Customised Complex short protocol test data generator.

    Adds a custom tests, with subcases for different wave shapes.
    """

    def __init__(self, fft_length) -> None:
        """FFT Generator specialisation of the ComplexShortBlockGenerator.

        Args:
            fft_length (int): FFT length as number of samples.
        """
        super().__init__(fft_length)
        self.fft_length = fft_length

    def _square_wave(self, number_of_samples, pulse_width, amplitude1, amplitude2):
        """Simple square wave or pulse generator.

        Limitations: Pulse and gap are same width.

        Args:
            number_of_samples (int): Number of samples to generate.
            pulse_width (int): Number of samples per pulse/gap.
            amplitude1 (int16): Amplitude of odd pulses.
            amplitude2 (int16): Amplitude of even pulses.

        Returns:
            Array of complex int samples.
        """
        data = [complex(0, 0)] * number_of_samples
        amplitudes = [complex(0, 0), amplitude1, complex(0, 0), amplitude2]
        for sample in range(number_of_samples):
            data[sample] = amplitudes[sample//pulse_width % 4]
        return data

    def _sawtooth_wave(self, number_of_samples, tooth_width, amplitude1, amplitude2, slope="normal"):
        """Simple sawtooth wave generator.

        Args:
            number_of_samples (int): Number of samples to generate.
            tooth_width (int): Number of samples per tooth.
            amplitude1 (int16): Amplitude at start of tooth.
            amplitude2 (int16): Amplitude at end of tooth.
            slope (str): normal, inverse, or triangle.

        Returns:
            Array of complex int samples.
        """
        data = [complex(0, 0)] * number_of_samples
        for sample in range(number_of_samples):
            if slope == "normal":
                height = (sample % tooth_width) / (tooth_width-1)
            elif slope == "inverse":
                height = 1 - (sample % tooth_width) / (tooth_width-1)
            elif slope == "triangle":
                offset = sample % tooth_width
                scale = tooth_width/2
                if offset < tooth_width/2:
                    height = offset / scale
                else:
                    height = (tooth_width-offset) / scale
            else:
                raise ValueError(f"Sawtooth slope={slope} invalid")
            data[sample] = amplitude1 + amplitude2 * height
        return data

    def _sine_waves(self, number_of_samples=None, periods=[1], amplitude=2**15):
        """Simple sine wave generator.

        Multiple period sine waves added and scaled.

        Limitations: Real and Imaginary are identical values.
                     All waves are the same amplitude.

        Args:
            number_of_samples (int): Number of samples to generate.
            periods (list of int): Number of samples per full cycle,
                for each sine wave to be accumulated.
            amplitude (int16): Maximum amplitude +/- from zero.

        Returns:
            Array of complex int samples.
        """
        if number_of_samples is None:
            number_of_samples = min(self.fft_length, self.MESSAGE_SIZE_LONGEST)

        data = np.zeros(number_of_samples, dtype=complex)
        for period in periods:
            data += (amplitude / len(periods)) * np.exp(complex(0, -2) *
                                                        np.pi * np.arange(number_of_samples) / period)
        return [complex(int(d.real), int(d.imag)) for d in data]

    def custom(self, seed, subcase):
        """Generate various waveforms.

        Usually called via ``self.generate()``.

        Subcase square: Simple maximum amplitude +/- pulses.

        Subcase square_inverse: As "square" but real/complex 100% out of phase

        Subcase sawtooth: Simple zero to maximum sawtooth.

        Subcase triangle: Simple maximum +/- triangle wave.

        Subcase sine: Simple single frequency maximum amplitude sine wave.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated
                for.

        Returns:
            Messages for the stated subcase.
        """
        random.seed(seed)

        number = self.fft_length * 2
        width = max(2, self.fft_length//64)
        positive = complex(2**15-1, 2**15-1)
        negative = complex(-2**15, -2**15)
        pos_neg = complex(2**15-1, -2**15)
        neg_pos = complex(-2**15, 2**15-1)

        if subcase == "square":
            messages = self._split_samples_into_opcodes(
                self._square_wave(number, width, positive, negative))
            return messages

        if subcase == "square_inverse":
            messages = self._split_samples_into_opcodes(
                self._square_wave(number, width, pos_neg, neg_pos))
            return messages

        if subcase == "sawtooth":
            messages = self._split_samples_into_opcodes(
                self._sawtooth_wave(number, width, complex(0, 0), positive, "normal"))
            return messages

        if subcase == "triangle":
            messages = self._split_samples_into_opcodes(
                self._sawtooth_wave(number, width, negative, positive, "triangle"))
            return messages

        if subcase == "sine":
            messages = self._split_samples_into_opcodes(
                self._sine_waves(number, [width], positive.real))
            return messages

        else:
            raise ValueError(f"Unexpected subcase of {subcase} for custom()")


arguments = get_generate_arguments(generator=BaseBlockGenerator)

subcase = os.environ["OCPI_TEST_subcase"]
inverse_fft = os.environ["OCPI_TEST_inverse_fft"]
indicate_start_of_fft_block = os.environ["OCPI_TEST_indicate_start_of_fft_block"].upper(
) == "TRUE"
increment_fft_block_value = os.environ["OCPI_TEST_increment_fft_block_value"].upper(
) == "TRUE"
start_of_fft_block_id = int(os.environ["OCPI_TEST_start_of_fft_block_id"])
start_of_fft_block_value = int(
    os.environ["OCPI_TEST_start_of_fft_block_value"])
fft_length = int(os.environ["OCPI_TEST_fft_length"])
gain_factor = int(os.environ["OCPI_TEST_gain_factor"])
metadata_fifo_size = int(os.environ["OCPI_TEST_metadata_fifo_size"])

seed = ocpi_testing.get_test_seed(arguments.case, subcase, inverse_fft,
                                  indicate_start_of_fft_block,
                                  increment_fft_block_value,
                                  start_of_fft_block_id, start_of_fft_block_value,
                                  fft_length, gain_factor,
                                  metadata_fifo_size)

generator = FastFourierTransformGenerator(fft_length=fft_length)
generator.SAMPLE_DATA_LENGTH = min(
    max(fft_length, generator.SAMPLE_DATA_LENGTH), generator.MESSAGE_SIZE_LONGEST)
generator.MESSAGE_SIZE_SHORTEST = 1

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "complex_short_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
