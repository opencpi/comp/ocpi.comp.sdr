.. fir_filter_scaled_worker_details

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

The FIR filter is split into sections, and acted upon by a number of multiply/accumulate (MAC) units, definable using the parameter ``number_of_multipliers``, during each clock cycle. This allows resource usage to be customised depending on the number of DSPs available and the required data rate: the more MACs, the higher the resource usage, but the lower the clock cycle count for data throughput.

In all cases, the input data is shifted through registers. The hardware used for the data is dependent on the tap/multiplier ratio: when equal, the registers are connected directly to the MAC; otherwise they are selected via a multiplexer. The hardware used for the tap constants is again dependent on the tap/multiplier ratio: when equal, tap data is placed into registers; when not equal, tap data is placed into block or distributed RAMs (therefore, in the case of distributed RAM, reducing the number of configurable logic blocks required). In order to ensure clock-to-clock timing is met, registers are placed between the data pipe and MACs, tap storage and MACs and on the outputs of the MAC multiply and accumulate blocks. In some cases these registers are created within the DSP blocks (used to create the MACs), and in others they are discrete.

For the first clock cycle, MAC(0) deals with tap(0), MAC(1) with tap(1) etc. On the next clock cycle MAC(0) deals with tap(number_of_multipliers), MAC(1) deals with tap(number_of_multipliers+1) etc. The value of a MAC after a particular clock cycle is given by :eq:`|component_name|-fir-filter-scaled-mac-equation`.

.. math::
   :label: |component_name|-fir-filter-scaled-mac-equation

   MAC_m = \sum_{k=0}^{N} h{(m+(k \times \text{number_of_multipliers}))} x[n-m-(k \times \text{number_of_multipliers})]

In :eq:`|component_name|-fir-filter-scaled-mac-equation`:

 * :math:`m` is the MAC unit (between 0 and ``number_of_multipliers`` - 1).

 * :math:`N` is the current clock cycle.

 * :math:`x[n]` is the input value at discrete time n.

 * :math:`h(k)` is the taps values.


The number of clock cycles required for the MACs to transit the filter, which is also the number of clock cycles between each data input is given by :eq:`|component_name|-fir-filter-scaled-clock-equation`.

.. math::
   :label: |component_name|-fir-filter-scaled-clock-equation

   \text{clock_cycles_between_inputs} = \left \lceil \frac{\text{number_of_taps}}{\text{number_of_multipliers}} \right \rceil

The output of each MAC is then added together to get the final filter output. If :math:`\text{clock_cycles_between_inputs} > 1` then part of this summation operation can be time shared over :math:`\text{clock_cycles_between_inputs}` clock cycles. In order to take advantage of this an array of serial adders are instantiated to perform the summation. The number of serial adders required is given by :eq:`|component_name|-fir-filter-scaled-adders-equation`.

.. math::
  :label: |component_name|-fir-filter-scaled-adders-equation

   \text{number_of_serial_adders} = \left \lceil \frac{\text{number_of_multipliers}}{\left \lceil \frac{\text{number_of_taps}}{\text{number_of_multipliers}} \right \rceil} \right \rceil

If :math:`\text{number_of_serial_adders} > 1` then the results of the serial adders also need to be summed to get the final filter result. This final operation is performed using a pipeline adder tree. A pipelined adder tree is typically resource intensive requiring instantiation of :math:`\text{number_of_serial_adders} - 1` adders.

This FIR filter design is most suited when

.. math::

   \left \lceil \frac{\text{number_of_taps}}{\text{number_of_multipliers}} \right \rceil \ge \text{number_of_multipliers}

as this does not require a pipeline adder tree. However it will operate with any combination of ``number_of_multipliers`` and ``number_of_taps``.

If :math:`\text{number_of_serial_adders} > 32`, then other HDL FIR implementations such as a systolic FIR filter may be more suitable: 128 taps and 64 multipliers would produce 32 serial adders and 31 adders (in a pipeline adder tree).
