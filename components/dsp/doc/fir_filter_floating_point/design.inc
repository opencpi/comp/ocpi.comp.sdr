.. fir_filter_floating_point_design

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

The mathematical representation of the implementation is given in :eq:`|component_name|-equation`.

.. math::
   :label: |component_name|-equation

   y[n] = \sum_{k=0}^{N-1} h[k] * x[n-k]

In :eq:`|component_name|-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`N` is the total number of taps.

 * :math:`h[k]` is the taps values, where there are :math:`N` taps index from :math:`0` to :math:`N - 1`.

A block diagram representation of the implementation is given in :numref:`|component_name|-diagram`.

.. _|component_name|-diagram:

.. figure:: |include_dir|fir_filter.svg
   :alt: Block diagram outlining finite impulse response (FIR) filter implementation.
   :align: center

   FIR filter implementation.
