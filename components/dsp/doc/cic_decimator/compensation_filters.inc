.. cic_decimator_compensation_filters

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

Passband attenuation is typically unwanted and can be compensated for by following the CIC decimator with an FIR filter. The frequency response of an idea compensation filter is shown in :eq:`|component_name|-cic_dec_comp_freq_resp-equation`.

.. math::
   :label: |component_name|-cic_dec_comp_freq_resp-equation

   H( \omega ) = \begin{cases}
   \left ( RM \right )^{N}\left | \frac{sin\left ( \frac{\omega }{2R} \right )}{sin\left ( \frac{\omega M}{2} \right )} \right | ^{N} & \omega \leq \omega_{cutoff} \\
   0 & \omega >  \omega_{cutoff}
   \end{cases}

Wideband Compensation
~~~~~~~~~~~~~~~~~~~~~
The passband frequency response of CIC filter with a compensation filter across the entire passband is shown in :numref:`|component_name|-cic_decimator_comp_freq_narrow-diagram`.

.. _|component_name|-cic_decimator_comp_freq_narrow-diagram:

.. figure:: |include_dir|cic_decimator_comp_freq_narrow.svg
   :alt: Narrowband frequency response of CIC with wideband compensation filter
   :align: center

   Narrowband frequency response of CIC with wideband compensation filter

Compensation across the entire passband is typically not recommended as it results in degraded rejection of aliasing signals compared to just a CIC decimator. This is shown in :numref:`|component_name|-cic_decimator_comp_freq_wide-diagram`.

.. _|component_name|-cic_decimator_comp_freq_wide-diagram:

.. figure:: |include_dir|cic_decimator_comp_freq_wide.svg
   :alt: Wideband frequency response of CIC with wideband compensation filter
   :align: center

   Wideband frequency response of CIC with wideband compensation filter

Narrowband Compensation
~~~~~~~~~~~~~~~~~~~~~~~
Narrowband compensation is when the CIC frequency response is only compensated for during part of the Nyquist bandwidth of the lower sample rate. Most systems use a narrowband compensation filter.

As a rule of thumb the cut-off frequency of the narrowband compensation filter should be :math:`F_{cutoff} \leq \frac{f_{s[low]}}{4M}`. If this condition is not met the performance of the CIC decimator, at some frequencies, will be negatively impacted by the compensation filter. This is shown in :numref:`|component_name|-cic_decimator_narrow_comp_narrow-diagram` and :numref:`|component_name|-cic_decimator_narrow_comp_wide-diagram`.

.. _|component_name|-cic_decimator_narrow_comp_narrow-diagram:

.. figure:: |include_dir|cic_decimator_narrow_comp_narrow.svg
   :alt: Narrowband Frequency Response of CIC with narrowband compensation filter
   :align: center

   Narrowband Frequency Response of CIC with narrowband compensation filter

.. _|component_name|-cic_decimator_narrow_comp_wide-diagram:

.. figure:: |include_dir|cic_decimator_narrow_comp_wide.svg
   :alt: Wideband Frequency Response of CIC with narrowband compensation filter
   :align: center

   Wideband Frequency Response of CIC with narrowband compensation filter

Generating Filter Taps
~~~~~~~~~~~~~~~~~~~~~~
The following python code can be used to generate the taps for an FIR compensation filter and to plot the system's frequency response.

.. code-block:: python

   from scipy.signal import firwin2
   from scipy.signal import freqz
   import numpy as np
   import matplotlib.pyplot as plt
   np.seterr(divide='ignore', invalid='ignore');

   # cutOff is the cut off freq as a fraction of the lower sample rate
   # i.e 0.5 = Nyquist frequency
   def getFIRCompensationFilter(R,M,N,cutOff,numTaps,calcRes=1024):
       w = np.arange(calcRes) * np.pi/(calcRes - 1)
       Hcomp = lambda w : ((M*R)**N)*(np.abs((np.sin(w/(2.*R))) /
                           (np.sin((w*M)/2.)) ) **N)
       cicCompResponse = np.array(list(map(Hcomp, w)))
       # Set DC response to 1 as it is calculated as 'nan' by Hcomp
       cicCompResponse[0] = 1
       # Set stopband response to 0
       cicCompResponse[int(calcRes*cutOff*2):] = 0
       normFreq = np.arange(calcRes) / (calcRes - 1)
       taps = firwin2(numTaps, normFreq, cicCompResponse)
       return taps

   def plotFIRCompFilter(R,M,N,taps,wideband=False):
       if wideband: # Interpolate FIR filter to higher sample rate
           interp = np.zeros(len(taps)*R)
           interp[::R] = taps
           freqs,compResponse = freqz(interp)
           w = np.arange(len(freqs)) * np.pi/len(freqs) * R
       else:
           freqs,compResponse = freqz(taps)
           w = np.arange(len(freqs)) * np.pi/len(freqs)
       Hcic = lambda w : (1/((M*R)**N))*np.abs( (np.sin((w*M)/2.)) / (np.sin(w/(2.*R))) )**N
       cicMagResponse = np.array(list(map(Hcic, w)))
       combinedResponse = cicMagResponse * compResponse
       plt.plot(freqs/(2*np.pi),20*np.log10(abs(cicMagResponse)), label="CIC Filter")
       plt.plot(freqs/(2*np.pi),20*np.log10(abs(compResponse)), label="Compensation Filter")
       plt.plot(freqs/(2*np.pi),20*np.log10(abs(combinedResponse)), label="Combined Response")
       plt.grid(); plt.legend();
       axes = plt.gca(); axes.set_ylim([-200,25])
       plt.show()
