-- HDL Implementation of a CIC interpolator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- CIC interpolator
-- Increases the sample rate by a user settable factor

-- The module supports back pressure being asserted on it. During backpressure
-- no input data is taken and no processing occurs.
-- The module will apply back pressure to allow for its processing time.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cic_int;
use sdr_dsp.sdr_dsp.rounding_halfup;

architecture rtl of worker is
  ----------------------------------------------------------------------------
  -- Constants
  ----------------------------------------------------------------------------
  -- Set input and output sizes to 16 bit
  constant input_word_size_c          : integer := 16;
  constant output_word_size_c         : integer := 16;
  -- Sets the size of the internal registers used to store the cic calculations
  constant cic_word_size_c            : integer := to_integer(cic_register_size);
  -- Sets number of comb and integrator stages
  constant comb_stages_c              : integer := to_integer(cic_order);
  constant int_stages_c               : integer := to_integer(cic_order);
  constant differential_delay_c       : integer := to_integer(cic_differential_delay);
  constant max_message_length_c       : integer := to_integer(unsigned(ocpi_max_bytes_output))/(input_in.data'length/8);
  constant flush_length_c             : integer := comb_stages_c*differential_delay_c-1;
  -- Sets delay through rounding
  constant rounding_delay_c           : integer := 3;
  -- Sets bit widths
  constant comb_stages_width_c        : integer := integer(ceil(log2(real(comb_stages_c+1))));
  constant differential_delay_width_c : integer := integer(ceil(log2(real(differential_delay_c+1))));
  ----------------------------------------------------------------------------
  -- Signals / Registers
  ----------------------------------------------------------------------------
  -- Data input signals
  signal data_in_i                    : signed(input_word_size_c - 1 downto 0);
  signal data_in_q                    : signed(input_word_size_c - 1 downto 0);
  -- Data output signals
  signal data_valid_out               : std_logic;
  signal data_out_i                   : signed(cic_word_size_c - 1 downto 0);
  signal data_out_q                   : signed(cic_word_size_c - 1 downto 0);
  -- Output rounding signals
  signal rounded_data_out_i           : signed(output_word_size_c - 1 downto 0);
  signal rounded_data_out_q           : signed(output_word_size_c - 1 downto 0);
  signal scale_factor                 : integer range 0 to 127;
  -- Interface  signals
  signal sample_interval_hold         : std_logic;
  signal input_hold                   : std_logic;
  signal upsample_enable              : std_logic;
  signal reset                        : std_logic;
  signal take                         : std_logic;
  signal data_valid_in                : std_logic;
  signal discontinuity                : std_logic;
  signal discontinuity_hold           : std_logic;
  signal processed_data               : std_logic_vector(output_out.data'range);
  signal processed_valid_in           : std_logic;
  signal input_interface              : worker_input_in_t;
  signal delay_interface              : worker_input_in_t;
  signal flush_length                 : unsigned(integer(ceil(log2(real(flush_length_c)))) downto 0);

begin

  ----------------------------------------------------------------------------
  -- Flush inject
  ----------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready.
  flush_length <= to_unsigned(flush_length_c, flush_length'length);

  flush_insert_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      -- disable max message length by setting to the max length of a flush
      max_message_length_g => max_message_length_c
      )
    port map (
      clk             => ctl_in.clk,
      reset           => reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  ----------------------------------------------------------------------------
  -- Interface handling (with eof and back pressure support)
  ----------------------------------------------------------------------------

  -- Input Control
  -- upsample_enable      = high when the upsample protocol
  --                        delay shift register is progressing
  -- sample_interval_hold = high when the protocol delay is
  --                        waiting for sample interval calculation
  -- discontinuity_hold   = high when discontinuity opcode is propagating
  --                        through complex short protocol delay

  upsample_enable <= not input_hold;

  take <= discontinuity when input_interface.opcode = complex_short_timed_sample_discontinuity_op_e
          else output_in.ready and upsample_enable and not sample_interval_hold
          and not discontinuity_hold;

  -- Split I and Q data
  data_in_i <= signed(input_interface.data(input_word_size_c - 1 downto 0));
  data_in_q <= signed(input_interface.data((2 * input_word_size_c) - 1 downto input_word_size_c));

  -- Sample opcode data valid in
  data_valid_in <= '1' when input_interface.opcode = complex_short_timed_sample_sample_op_e
                   and input_interface.valid = '1' and sample_interval_hold = '0'
                   else '0';

  -- Interface Masking Process
  -- Prevents the progressing of primitives when waiting for
  -- processing to complete elsewhere by setting valid/ready to 0.
  -- This keep delays in sync between primitives.
  mask_interface_p : process(input_interface, sample_interval_hold)
  begin
    delay_interface <= input_interface;
    -- valid/ready masking for protocol delay timing during sample_interval
    if sample_interval_hold = '1' then
      delay_interface.valid <= '0';
      delay_interface.ready <= '0';
    end if;
  end process;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the CIC interpolator.
  interface_delay_i : entity work.complex_short_protocol_delay
    generic map (
      data_in_width_g      => input_in.data'length,
      -- delay due to comb stage
      stage1_delay_g       => comb_stages_c,
      -- delay due to integrator stage and rounding
      stage2_delay_g       => (int_stages_c + rounding_delay_c),
      max_message_length_g => max_message_length_c
      )
    port map (
      clk                  => ctl_in.clk,
      reset                => ctl_in.reset,
      enable               => output_in.ready,
      upsample_enable      => upsample_enable,
      sample_interval_hold => sample_interval_hold,
      pd_discontinuity     => discontinuity,
      discontinuity_hold   => discontinuity_hold,
      input_in             => delay_interface,
      interpolation_factor => props_in.up_sample_factor,
      processed_stream_in  => processed_data,
      processed_valid_in   => processed_valid_in,
      output_out           => output_out
      );

  ----------------------------------------------------------------------------
  -- CIC filter
  ----------------------------------------------------------------------------
  -- CIC interpolator reset signal
  reset <= ctl_in.reset or discontinuity or props_in.up_sample_factor_written;

  -- Instantiate two CIC modules for I and Q streams
  cic_module_i : cic_int
    generic map (
      int_stages_g         => int_stages_c,
      comb_stages_g        => comb_stages_c,
      differential_delay_g => differential_delay_c,
      input_word_size_g    => input_word_size_c,
      output_word_size_g   => cic_word_size_c
      )
    port map (
      clk              => ctl_in.clk,
      reset            => reset,
      clk_en           => output_in.ready,
      data_valid_in    => data_valid_in,
      data_eom_in      => input_interface.eom,
      data_in          => data_in_i,
      up_sample_factor => props_in.up_sample_factor,
      input_hold       => input_hold,
      data_valid_out   => data_valid_out,
      data_eom_out     => open,
      data_out         => data_out_i
      );

  cic_module_q : cic_int
    generic map (
      int_stages_g         => int_stages_c,
      comb_stages_g        => comb_stages_c,
      differential_delay_g => differential_delay_c,
      input_word_size_g    => input_word_size_c,
      output_word_size_g   => cic_word_size_c
      )
    port map (
      clk              => ctl_in.clk,
      reset            => reset,
      clk_en           => output_in.ready,
      data_valid_in    => data_valid_in,
      data_eom_in      => '0',
      data_in          => data_in_q,
      up_sample_factor => props_in.up_sample_factor,
      input_hold       => open,
      data_valid_out   => open,
      data_eom_out     => open,
      data_out         => data_out_q
      );

  ----------------------------------------------------------------------------
  -- Output rounding and scaling
  ----------------------------------------------------------------------------
  -- Round output using half-up adder

  -- Get scale factor from property
  scale_factor <= to_integer(props_in.scale_output);

  halfup_rounder_i : rounding_halfup
    generic map (
      input_width_g   => cic_word_size_c,
      output_width_g  => output_word_size_c,
      saturation_en_g => false,
      registers_g     => rounding_delay_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => reset,
      clk_en         => output_in.ready,
      data_in        => data_out_i,
      data_out       => rounded_data_out_i,
      binary_point   => scale_factor,
      data_valid_in  => data_valid_out,
      data_valid_out => processed_valid_in
      );

  halfup_rounder_q : rounding_halfup
    generic map (
      input_width_g   => cic_word_size_c,
      output_width_g  => output_word_size_c,
      saturation_en_g => false,
      registers_g     => rounding_delay_c
      )
    port map(
      clk            => ctl_in.clk,
      reset          => reset,
      clk_en         => output_in.ready,
      data_in        => data_out_q,
      data_out       => rounded_data_out_q,
      binary_point   => scale_factor,
      data_valid_in  => data_valid_out,
      data_valid_out => open
      );

  processed_data <= std_logic_vector(rounded_data_out_q) &
                    std_logic_vector(rounded_data_out_i);

end rtl;
