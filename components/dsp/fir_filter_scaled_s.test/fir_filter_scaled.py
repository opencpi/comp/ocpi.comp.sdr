#!/usr/bin/env python3

# Runs checks for fir_filter_scaled_s testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Short FIR Filter Scaled implementation for verification."""

from sdr_interface.sample_time import SampleTime
import opencpi.ocpi_testing as ocpi_testing
import opencpi.ocpi_protocols as ocpi_protocols
import numpy

from sdr_dsp.fir_filter_scaled import FirFilterScaled


class FirFilterScaled_Short(ocpi_testing.Implementation):
    """Short FIR Filter Scaled implementation for verification."""

    def __init__(self, taps, scale_factor, rounding_type, overflow_type,
                 group_delay_seconds=0, group_delay_fractional=0,
                 max_message_samples=ocpi_protocols.PROTOCOLS[
                     "short_timed_sample"].max_sample_length):
        """Initialises a short FIR filter (scaled) class.

        Args:
            taps (list of float): Tap coefficient values.
            scale_factor (int): Number of bits to be removed from calculated output.
                                  If input is 16 bit, tap values are 16 bit and there are 64 taps, total output size is 16 + 16 + 8 = 40 bits.
                                  If scale_factor = 10, output data will be calculated output(25:10).
            rounding_type (string): Rounding method to use when scaling.
            overflow_type (string): How to overflow result.
            group_delay_seconds (int): Filter group-delay in whole seconds.
            group_delay_fractional (int): Filter group-delay fractional part multiplied by 2^64.
            max_message_samples (int): Maximum number of samples in a message.
        """
        super().__init__(_max_message_samples=max_message_samples)

        self._filter = FirFilterScaled(taps=taps,
                                       scale_factor=scale_factor,
                                       rounding_type=rounding_type,
                                       overflow_type=overflow_type,
                                       output_depth=16)
        self._number_of_taps = len(taps)

        # Instantiate SampleTime class
        self._sample_time = SampleTime(
            group_delay_seconds=group_delay_seconds,
            group_delay_fractional=group_delay_fractional)

    def reset(self):
        """Reset to initialisation state."""
        self._filter.reset()

    def sample(self, input_):
        """Sample opcode functionality.

        Calls _sample function to process samples.

        Args
            input_ (list of short): Sample opcode data for the input port.

        Returns:
            Formatted messages produced on receipt of the sample opcode.
        """
        return self.output_formatter(self._sample(input_))

    def _sample(self, values):
        """Process incoming sample values.

        Calls run_filter function to apply filter to sample values.

        Args
            values (list): Sample values to be filtered.

        Returns:
            List of messages produced.
        """
        output = self._filter.run_filter(values)
        output = numpy.int16(output)
        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]
        return messages

    def flush(self, value):
        """Process zero sample values to flush out any in-progress values.

        If there is any in-progress data, flush it out by processing tap x zeros, forwarding on any sample opcode messages produced.
        Then forward the flush opcode.

        Returns:
            List of messages produced on receipt of the flush opcode.
        """
        messages = []
        if self._filter.data_received:
            flush_data = [0] * self._number_of_taps
            messages = self._sample(flush_data)
        messages.append({"opcode": "flush", "data": None})
        self.reset()

        return self.output_formatter(messages)

    def discontinuity(self, value):
        """Resets filter and forwards opcode.

        Returns:
            List of messages produced on receipt of the flush opcode.
        """
        self.reset()
        messages = [{"opcode": "discontinuity", "data": None}]

        return self.output_formatter(messages)

    def time(self, value):
        """Process time opcode.

        Calls SampleTime function, to subtract group_delay_seconds/fractional from current time opcode value..

        Args:
            value (decimal): Opcode data for the input port.

        Returns:
            List of messages produced on receipt of the time opcode.
        """
        self._sample_time.time = value
        messages = [
            {"opcode": "time", "data": self._sample_time.opcode_time()}]
        return self.output_formatter(messages)
