-- frequency_modulator_l_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cordic_dds;

architecture rtl of worker is
  -- Delay to align data with control flags
  constant delay_c                : integer := 16;
  constant output_size_c          : integer := 16;  -- i & q data widths
  constant dds_phase_accum_size_c : integer := 32;  -- Width of DDS

  signal dds_step    : std_logic_vector(dds_phase_accum_size_c - 1 downto 0);
  signal dds_sin_out : std_logic_vector(output_size_c - 1 downto 0);
  signal dds_cos_out : std_logic_vector(output_size_c - 1 downto 0);
  signal data_out    : std_logic_vector((output_size_c * 2) - 1 downto 0);

begin

  -- take input when both input and output are ready
  input_out.take <= output_in.ready;

  dds_step <= input_in.data when input_in.valid = '1' and
              input_in.opcode = long_timed_sample_sample_op_e else (others => '0');

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the cordic_dds module.
  interface_delay_i : entity work.long_to_complex_short_protocol_delay
    generic map (
      delay_g              => delay_c,
      data_in_width_g      => input_in.data'length,
      processed_data_mux_g => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_in.ready,
      take_in             => '1',
      input_in            => input_in,
      processed_stream_in => data_out,
      output_out          => output_out
      );

  -- DDS module
  dds_inst : cordic_dds
    generic map (
      output_size_g      => output_size_c,
      phase_accum_size_g => dds_phase_accum_size_c
      )
    port map (
      clk        => ctl_in.clk,
      rst        => ctl_in.reset,
      clk_en     => output_in.ready,
      step_size  => dds_step,
      gain_in    => std_logic_vector(props_in.modulator_amplitude),
      sine_out   => dds_sin_out,
      cosine_out => dds_cos_out
      );

  data_out <= dds_sin_out & dds_cos_out;

end rtl;
