.. fir_filter_xf documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fir_filter_xf:


Finite Impulse Response (FIR) Filter (``fir_filter_xf``)
========================================================
Applies finite impulse response (FIR) filter to a stream of values.

Design
------
.. ocpi_documentation_include:: ../doc/fir_filter_floating_point/design.inc

   |component_name|: fir_filter_xf
   |include_dir|: ../doc/fir_filter_floating_point/

The real and imaginary values are filtered independently using the same tap values.

Interface
---------
.. literalinclude:: ../specs/fir_filter_xf-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
The FIR filter is applied to values in sample opcode messages.

After a flush opcode message is received, the filter buffer is flushed. This is done by generating an input of zeros, the length of which is set in the ``number_of_taps`` property. After the flush has been completed and the resulting sample opcode message sent, the flush message is then forwarded on.

When a discontinuity opcode message is received the internal buffers are reset to the initialisation state and the discontinuity opcode is forwarded on.

When a time opcode message is received the time is decremented by the group delay value before it is forwarded on.

All other opcodes are passed through this component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../fir_filter_xf.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
.. ocpi_documentation_include:: ../doc/fir_filter_floating_point/dependencies.inc

Limitations
-----------
.. ocpi_documentation_include:: ../doc/fir_filter_floating_point/limitations.inc

   |component_name|: fir_filter_xf

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
