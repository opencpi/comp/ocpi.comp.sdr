// RCC implementation of fir_filter_f worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../../math/common/time_utils.hh"
#include "../common/fir/fir_core.hh"
#include "fir_filter_f-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fir_filter_fWorkerTypes;

class Fir_filter_fWorker : public Fir_filter_fWorkerBase {
  // fir_core <SAMPLE_TYPE, ACCUMULATOR_TYPE>
  // Calculate using the same precision/type as the input.
  fir_core<RCCFloat, RCCFloat> fir{};

  uint32_t group_delay_time_seconds = 0;   // group delay time seconds part
  uint64_t group_delay_time_fraction = 0;  // group delay time fractional part
  bool data_flushed = false;  // Flags when flush data has been handled, and
                              // flush opcode should be passed
  bool data_received = false;
  const RCCFloat zero_input_data[UINT8_MAX] = {0};

  RCCResult start() {
    if (FIR_FILTER_F_NUMBER_OF_TAPS == 0) {
      setError("Number of taps must be > 0");
      return RCC_FATAL;
    }

    // The FIR Filter Float expects the
    // output to not be saturated and to wrap around
    // the max or min of the output type.
    sdr_math::overflow_type overflow{sdr_math::overflow_type::MATH_WRAP};

    this->fir.set(FIR_FILTER_F_NUMBER_OF_TAPS, properties().taps,
                  sdr_math::rounding_type::MATH_HALF_EVEN, overflow);

    return RCC_OK;
  }

  // Notification that taps property has been written
  RCCResult taps_written() {
    this->fir.set_taps(FIR_FILTER_F_NUMBER_OF_TAPS, properties().taps);
    return RCC_OK;
  }

  // Notification that group_delay_seconds property has been written
  RCCResult group_delay_seconds_written() {
    this->group_delay_time_seconds = properties().group_delay_seconds;
    return RCC_OK;
  }

  // Notification that group_delay_fractional property has been written
  RCCResult group_delay_fractional_written() {
    this->group_delay_time_fraction = properties().group_delay_fractional;
    return RCC_OK;
  }

  RCCResult run(bool) {
    // Handle the most common opCode first
    if (input.opCode() == Float_timed_sampleSample_OPERATION) {
      const RCCFloat *input_data = input.sample().data().data();
      RCCFloat *output_data = output.sample().data().data();

      output.setOpCode(Float_timed_sampleSample_OPERATION);
      if (input.length() == 0) {
        output.setLength(input.sample().data().size());
      } else {
        output.setLength(input.sample().data().size() * sizeof(RCCFloat));
        this->fir.do_work(input_data, input.sample().data().size(),
                          output_data);
        this->data_received = true;
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Float_timed_sampleTime_OPERATION);
      uint32_t output_time_seconds = input.time().seconds();
      uint64_t output_time_fraction = input.time().fraction();
      // Subtract group delay from time.
      time_utils::subtract(&output_time_seconds, &output_time_fraction,
                           this->group_delay_time_seconds,
                           this->group_delay_time_fraction);
      output.time().seconds() = output_time_seconds;
      output.time().fraction() = output_time_fraction;
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Float_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleFlush_OPERATION) {
      if (this->data_flushed == false && this->data_received) {
        // When a flush is requested input the set number of zeros
        const RCCFloat *input_data = this->zero_input_data;
        output.setOpCode(Float_timed_sampleSample_OPERATION);
        RCCFloat *output_data = output.sample().data().data();
        // As the maximum flush length is 255 (uint8_t max) this will fit into
        // a single message.
        output.setLength(FIR_FILTER_F_NUMBER_OF_TAPS * sizeof(RCCFloat));
        this->fir.do_work(input_data, FIR_FILTER_F_NUMBER_OF_TAPS, output_data);
        output.advance();  // Advance output only as no more input data is
                           // needed until the flush opcode is sent.
        this->data_flushed = true;
        return RCC_OK;
      }
      // Pass through flush opcode
      output.setOpCode(Float_timed_sampleFlush_OPERATION);
      this->data_flushed = false;
      this->data_received = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleDiscontinuity_OPERATION) {
      this->fir.reset();
      // Pass through discontinuity opcode
      output.setOpCode(Float_timed_sampleDiscontinuity_OPERATION);
      this->data_received = false;
      return RCC_ADVANCE;
    } else if (input.opCode() == Float_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Float_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

FIR_FILTER_F_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FIR_FILTER_F_END_INFO
