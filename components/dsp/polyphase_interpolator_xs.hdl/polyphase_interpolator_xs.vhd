-- HDL Implementation of polyphase interpolator
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


library ocpi;
use ocpi.types.all;  -- remove this to avoid all ocpi name collisions

library sdr_dsp;
use sdr_dsp.sdr_dsp.polyphase_interpolator;

architecture rtl of worker is

  constant data_width_c     : integer := input_in.data'length/2;
  constant num_taps_c       : integer := integer(to_integer(max_number_taps));
  constant num_taps_width_c : integer := integer(ceil(log2(real(num_taps_c))));

  signal take            : std_logic;
  signal input_interface : worker_input_in_t;
  signal no_data         : std_logic;

  signal flush_out            : std_logic;
  -- The flush length might need to flush the whole of the ram block, therefore
  -- needs to handle the case where the `num_taps_width_c` is a power of 2 value
  signal flush_length         : unsigned(num_taps_width_c downto 0);
  signal discontinuity        : std_logic;
  signal interpolation_factor : unsigned(num_taps_width_c-1 downto 0);

  -- a `num_taps_in` of 0 is acceptable (and means that all the taps are set).
  signal num_taps_in : unsigned(num_taps_width_c-1 downto 0);
  signal tap_addr    : unsigned(num_taps_width_c-1 downto 0);
  signal tap_data    : signed(15 downto 0);
  signal tap_valid   : std_logic;

  signal pd_hold            : std_logic;
  signal data_valid_in      : std_logic;
  signal input_ready        : std_logic;
  signal interpolator_ready : std_logic;

  signal output_data_real  : signed(data_width_c-1 downto 0);
  signal output_data_imag  : signed(data_width_c-1 downto 0);
  signal output_data_valid : std_logic;
  signal processed_data    : std_logic_vector((2*data_width_c)-1 downto 0);
  signal processed_last    : std_logic;

  function rounding_type_to_string(rounding_type : in work.polyphase_interpolator_xs_constants.rounding_type_t)
    return string is
  begin
    if rounding_type = truncate_e then
      return "truncate";
    elsif rounding_type = half_up_e then
      return "half_up";
    elsif rounding_type = half_even_e then
      return "half_even";
    end if;
    report "Bad rounding mode requested" severity failure;
    return "truncate";
  end function;

begin

  -- props_out set to default
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= btrue;
  props_out.raw.error <= bfalse;

  ------------------------------------------------------------------------------
  -- Interface handling
  ------------------------------------------------------------------------------
  interpolator_ready <= input_ready;
  take               <= interpolator_ready and output_in.ready and not pd_hold;

  data_valid_in <= '1' when (input_interface.opcode = complex_short_timed_sample_sample_op_e
                             and input_interface.valid = '1' and pd_hold = '0')
                   else '0';

  discontinuity <= '1' when (input_interface.ready = '1') and (take = '1')
                   and (input_in.opcode = complex_short_timed_sample_discontinuity_op_e)
                   else '0';

  flush_out <= '1' when (input_interface.ready = '1' and take = '1'
                         and input_interface.opcode = complex_short_timed_sample_flush_op_e)
               else '0';

  flush_length <= (others => '0') when no_data = '1' else resize((props_in.number_of_taps), flush_length'length);

  no_data_flag : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        no_data <= '1';
      else
        if take = '1' and data_valid_in = '1' then
          no_data <= '0';
        end if;
        if flush_out = '1' then
          no_data <= '1';
        end if;
      end if;
    end if;
  end process;

  flush_inserter_i : entity work.complex_short_flush_injector
    generic map (
      data_in_width_g      => input_in.data'length,
      max_message_length_g => to_integer(ocpi_max_bytes_output/4)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  interface_delay_i : entity work.complex_short_protocol_delay
    generic map (
      data_in_width_g => input_in.data'length
      )
    port map (
      clk                    => ctl_in.clk,
      reset                  => ctl_in.reset,
      enable                 => output_in.ready,
      take_in                => interpolator_ready,
      pd_hold                => pd_hold,
      input_in               => input_interface,
      interpolation_factor   => props_in.interpolation_factor,
      group_delay_seconds    => props_in.group_delay_seconds,
      group_delay_fractional => props_in.group_delay_fractional,
      processed_stream_in    => processed_data,
      processed_valid_in     => output_data_valid,
      processed_end_in       => processed_last,
      output_out             => output_out
      );

  ------------------------------------------------------------------------------
  -- Interpolation
  ------------------------------------------------------------------------------

  -- Tap data is only 16 bits, therefore there are 2 tap messages possible
  -- on the raw interface
  tap_data <= signed(props_in.raw.data(15 downto 0)) when props_in.raw.byte_enable(0) = '1'
              else signed(props_in.raw.data(31 downto 16));

  -- Plus one offset as byte addressing is being converted to short
  -- (even though the data is bytes)
  tap_addr <= resize(shift_right(unsigned(props_in.raw.address), 1), num_taps_width_c);

  -- only allow tap valid high if the address is in range
  tap_valid <= '1' when (props_in.raw.is_write and
                         props_in.raw.address(31 downto 1) < (max_number_taps))
               else '0';

  interpolation_factor <= resize(props_in.interpolation_factor, num_taps_width_c);
  num_taps_in          <= resize(unsigned(props_in.number_of_taps), num_taps_width_c);

  poly_int_real : sdr_dsp.sdr_dsp.polyphase_interpolator
    generic map (
      complex_data_g  => true,
      single_branch_g => false,
      data_width_g    => data_width_c,
      max_num_taps_g  => to_integer(max_number_taps),
      rounding_mode_g => rounding_type_to_string(rounding_type)
      )
    port map (
      clk                  => ctl_in.clk,
      reset                => ctl_in.reset,
      data_reset           => discontinuity,
      enable               => output_in.ready,
      -- Control Signals
      interpolation_factor => interpolation_factor,
      num_taps_in          => num_taps_in,
      run_single_in        => '0',
      branch_in            => (others => '0'),
      -- Tap RAM signals
      tap_data_in          => tap_data,
      tap_addr_in          => tap_addr,
      tap_valid_in         => tap_valid,
      tap_ready_out        => open,
      -- Input Stream signals
      input_data_i_in      => signed(input_interface.data(data_width_c-1 downto 0)),
      input_data_q_in      => signed(input_interface.data(2*data_width_c-1 downto data_width_c)),
      input_valid_in       => data_valid_in,
      input_last_in        => input_interface.eom,
      input_ready_out      => input_ready,
      -- Output Stream signals
      output_data_i_out    => output_data_real,
      output_data_q_out    => output_data_imag,
      output_valid_out     => output_data_valid,
      output_last_out      => processed_last,
      output_ready_in      => '1'
      );

  processed_data <= std_logic_vector(output_data_imag) & std_logic_vector(output_data_real);

end rtl;
