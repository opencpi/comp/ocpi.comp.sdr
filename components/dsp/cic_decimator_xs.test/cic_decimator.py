#!/usr/bin/env python3

# Python implementation of CIC filter, with decimator.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Cascaded Integrator-Comb Decimator complex short implementation."""

import numpy as np
from sdr_interface.sample_time import SampleTime
from sdr_dsp.cic_decimator import CicDecimatorSingleChannel

import opencpi.ocpi_testing as ocpi_testing
import opencpi.ocpi_protocols as ocpi_protocols


class CicDecimatorComplexShort(ocpi_testing.Implementation):
    """Cascaded Integrator-Comb Decimator reference implementation."""

    def __init__(self, cic_order, delay_factor, down_sample_factor,
                 output_scale_factor, cic_register_size):
        """Cascaded Integrator-Comb Decimator class.

        Args:
            cic_order (integer): Number of Comb and Integrator stages present.
            delay_factor (integer): Comb delay factor.
            down_sample_factor (integer): output downsample ratio.
            output_scale_factor (integer): output scaling factor.
            cic_register_size (integer): bit depth of the CIC integers used internally.

        Returns:
            Instance of the CicDecimatorSingleChannel Class.
        """
        super().__init__(cic_order=cic_order,
                         delay_factor=delay_factor,
                         down_sample_factor=down_sample_factor,
                         output_scale_factor=output_scale_factor,
                         cic_register_size=cic_register_size)

        self._max_message_samples = ocpi_protocols.PROTOCOLS[
            "complex_short_timed_sample"].max_sample_length

        self._sample_counter = self.down_sample_factor
        self._sample_time = SampleTime(
            interval_down_sampling_factor=self.down_sample_factor)
        self._messages_buffer = []
        self.samples_received = False

        self._cic_real = CicDecimatorSingleChannel(
            self.cic_order, self.delay_factor, self.down_sample_factor,
            self.output_scale_factor, self.cic_register_size)
        self._cic_imag = CicDecimatorSingleChannel(
            self.cic_order, self.delay_factor, self.down_sample_factor,
            self.output_scale_factor, self.cic_register_size)

    def reset(self):
        """Clears all internal buffers."""
        self._cic_real.reset()
        self._cic_imag.reset()
        self._sample_time.time = None
        self._sample_counter = 0
        self.samples_received = False
        self._messages_buffer = []

    def _handle_samples(self, values):
        """Process samples into output messages.

        Performs cic decimation on samples, adding any held timestamp which
        is incremented for incoming samples, and maintains a decimation
        count.

        Args:
            values (list): sample message contents.

        Returns:
            Array of messages.
        """
        input_data = np.array(values)
        output_real = self._cic_real.sample(np.real(input_data))
        output_imag = self._cic_imag.sample(np.imag(input_data))
        output = []
        messages = []

        if len(output_real):
            output = list(np.vectorize(complex)(output_real, output_imag))

        if self._sample_time.time is not None:
            if len(output) == 0:
                # Not enough samples to complete decimation, just increment
                # time by samples received.
                self._sample_time.advance_time_by(len(values))
            else:
                # Enough sample data has been received to complete a
                # decimation and output a sample.
                messages += [{"opcode": "sample", "data": output[:1]}]

                # Now output the held time.
                self._sample_time.advance_time_by(
                    self.down_sample_factor-self._sample_counter)
                messages += [{"opcode": "time",
                              "data": self._sample_time.opcode_time()}]
                self._sample_time.time = None

                # Add remainder of decimated output samples.
                if len(output) > 1:
                    messages += [{"opcode": "sample", "data": output[1:]}]

        elif len(output_real):
            messages.append({"opcode": "sample", "data": output})
            self._messages_buffer.clear()

        # Calculate the number of samples remaining after inputs have been processed
        self._sample_counter += len(values)
        self._sample_counter %= self.down_sample_factor

        return messages

    def sample(self, values):
        """Process an incoming message with the sample opcode.

        Args:
            values (list): sample message contents.

        Returns:
            Array of message dictionaries.
        """
        self.samples_received = True
        return self.output_formatter(self._handle_samples(values))

    def flush(self, *inputs):
        """Process an incoming message with the flush opcode.

        Args:
            inputs (list): ignored.

        Returns:
            Array of message dictionaries.
        """
        messages = []
        if self.samples_received:
            # Calculate how many samples are required to clear
            # the internal buffers, ignoring decimation boundary.
            flush_length = self.cic_order * \
                ((self.down_sample_factor * self.delay_factor) - 1)

            # Calculate how many additional samples are
            # required to reach a decimation boundary.
            samples_to_decimation_boundary = self.down_sample_factor - \
                ((flush_length + self._sample_counter) % self.down_sample_factor)

            # To stop there being no extra zeros when it is not necessary
            if samples_to_decimation_boundary != self.down_sample_factor:
                flush_length += samples_to_decimation_boundary

            if flush_length > 0:
                # Run sample method on flush samples. Split input message over
                # multiple messages if required.
                while flush_length > 0:
                    if flush_length > self._max_message_samples:
                        messages += self._handle_samples(
                            [0] * self._max_message_samples)
                        flush_length -= self._max_message_samples
                    else:
                        messages += self._handle_samples([0] * flush_length)
                        flush_length = 0

        messages.append({"opcode": "flush", "data": None})
        self.samples_received = False
        return self.output_formatter(messages)

    def sample_interval(self, value):
        """Process an incoming message with the sample_interval opcode.

        Args:
            value (decimal): incoming sample interval value.

        Returns:
            Array of message dictionaries.
        """
        self._sample_time.interval = value
        return self.output_formatter(
            [{"opcode": "sample_interval", "data": self._sample_time.opcode_interval()}])

    def time(self, value):
        """Process an incoming message with the time opcode.

        If not processing a decimation then forward timestamp, else hold timestamp
        until next sample output.

        Args:
            value (decimal): incoming time value.

        Returns:
            Empty list of message dictionaries.
        """
        messages = []
        if self._sample_counter == 0:
            messages = [{"opcode": "time", "data": value}]
        else:
            self._sample_time.time = value

        return self.output_formatter(messages)

    def discontinuity(self, *inputs):
        """Handle discontinuity opcode.

        Args:
            inputs (list): ignored.

        Returns:
            Formatted Discontinuity message.
        """
        messages = []
        if self._sample_time.time is not None:
            messages += [{"opcode": "time",
                          "data": self._sample_time.opcode_time()}]
        self.reset()

        messages += [{"opcode": "discontinuity", "data": None}]
        return self.output_formatter(messages)

    def end_of_files(self):
        """Generate any additional messages once the inputs have ended.

        Returns:
            Formatted messages.
        """
        messages = []
        if self._sample_time.time is not None:
            messages += [{"opcode": "time",
                          "data": self._sample_time.opcode_time()}]
        return self.output_formatter(messages)
