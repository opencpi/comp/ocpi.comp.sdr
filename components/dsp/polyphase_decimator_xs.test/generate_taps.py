#!/usr/bin/env python3

# This file is used to generate taps for the unit tests.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Script used to generate taps for the unit tests."""

import os
import sys
import logging
import scipy.signal as sig
import numpy as np
import opencpi.ocpi_testing as ocpi_testing

taps_per_branch = int(os.environ["OCPI_TEST_taps_per_branch"])
decimation_factor = int(os.environ["OCPI_TEST_decimation_factor"])
taps_shape = os.environ["OCPI_TEST_taps_shape"]

log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"
logging.basicConfig(level=verify_log_level)

# Parse arguments
taps_type = sys.argv[1]
taps_fractional_bits = int(sys.argv[2])
output_path = sys.argv[3]

total_taps = taps_per_branch*decimation_factor

logging.debug("Generating {} taps with {} window"
              .format(total_taps, taps_shape))

seed = ocpi_testing.get_test_seed(taps_per_branch, decimation_factor, taps_shape,
                                  taps_type, taps_fractional_bits)

if taps_shape == "kaiser":
    if decimation_factor <= 1:
        taps = (decimation_factor * sig.firwin(
            taps_per_branch,
            1/(taps_per_branch),
            window=("kaiser", 5.0)))
    else:
        taps = (decimation_factor * sig.firwin(
            decimation_factor*taps_per_branch,
            1/(decimation_factor),
            window=("kaiser", 5.0)))
elif taps_shape == "all_ones":
    taps = np.ones(taps_per_branch*decimation_factor)
elif taps_shape == "random":
    np.random.seed(seed)
    # Generate taps in the open range (-1,+1)
    # N.B. Random returns [min,max) so will need to ensure bounds
    max_val = +1
    min_val = -1
    # Random returns values up to, but not including, max_val, but
    # does return down to *and* including min_val.
    # Need to not have exactly -UINT_MAX
    min_val += sys.float_info.epsilon
    taps = np.random.uniform(min_val, max_val,
                             size=(taps_per_branch*decimation_factor))
else:
    assert False, f"Unrecognised taps_shape: {taps_shape}"

# Scale to integer format
integer_taps = [int((x/max(taps))*(pow(2, taps_fractional_bits)-2))
                for x in taps]

assert (np.all(np.array(integer_taps) < 2**taps_fractional_bits)
        and np.all(np.array(integer_taps) > -2**taps_fractional_bits)), (
    f"Filter taps goes beyond the range allowed for {taps_type} "
    f"({-(2**taps_fractional_bits)-1}-{(2**taps_fractional_bits)-1})"
    f"\nTaps: {integer_taps}")

with open(output_path, "w") as f:
    for tap in integer_taps:
        f.write(f"{tap}\n")
