#!/usr/bin/env python3

# Python implementation of Polyphase Decimator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Polyphase Decimator verification script."""

import os
import sys
import logging

import opencpi.ocpi_testing as ocpi_testing

from polyphase_decimator import PolyphaseDecimator_XS

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

test_id = ocpi_testing.get_test_case(input_file_path)

# Initialise Decimator object
log_level = int(os.environ.get("OCPI_LOG_LEVEL", 1))
decimation_factor = int(os.environ["OCPI_TEST_decimation_factor"])
max_decimation_factor = int(os.environ.get(
    "OCPI_TEST_max_decimation", decimation_factor))

num_taps_per_branch = int(os.environ["OCPI_TEST_taps_per_branch"])
num_taps = num_taps_per_branch * decimation_factor
max_taps_per_branch = int(os.environ.get(
    "OCPI_TEST_max_taps_per_branch", num_taps))

rounding_type = os.environ["OCPI_TEST_rounding_type"]
group_delay_seconds = int(os.environ["OCPI_TEST_group_delay_seconds"])
group_delay_fractional = int(os.environ["OCPI_TEST_group_delay_fractional"])

filter_taps = [0] * (num_taps_per_branch * decimation_factor)
for i, x in enumerate((os.environ["OCPI_TEST_taps"]).split(",")):
    if i < (num_taps_per_branch * decimation_factor):
        filter_taps[i] = int(x)
    else:
        if int(x) != 0:
            raise ValueError("Taps contains too many values (expected: {}, got: {})\n{}"
                             .format(num_taps_per_branch * decimation_factor,
                                     len((os.environ["OCPI_TEST_taps"]).split(
                                         ",")),
                                     (os.environ["OCPI_TEST_taps"]).split(",")))

if log_level > 9:
    verify_log_level = "DEBUG"
elif log_level > 5:
    verify_log_level = "INFO"
else:
    verify_log_level = "WARNING"

logfile = os.path.dirname(output_file_path)
logfile = os.path.join(logfile, f"{test_id}.polyphase_decimator_xs.py.log")
logging.basicConfig(level=verify_log_level, filename=logfile, filemode="w")


decimator_implementation = PolyphaseDecimator_XS(
    decimation_factor=decimation_factor,
    max_decimation=max_decimation_factor,
    taps=filter_taps,
    rounding_type=rounding_type,
    group_delay_seconds=group_delay_seconds,
    group_delay_fractional=group_delay_fractional,
    logging_level=verify_log_level)

# Check that the file isn't empty
if (os.stat(output_file_path).st_size == 0):
    print("Error: Empty output file")
    sys.exit(1)

# Check the result
verifier = ocpi_testing.Verifier(decimator_implementation)
verifier.set_port_types(["complex_short_timed_sample"],
                        ["complex_short_timed_sample"],
                        ["equal"])
verifier.comparison[0].TIME_FRACTIONAL_BITS_PRECISION = 48
logging.info("Only checking {} bits of time fractional values.".format(
    verifier.comparison[0].TIME_FRACTIONAL_BITS_PRECISION))

if not verifier.verify(test_id, [input_file_path], output_file_path):
    sys.exit(1)
