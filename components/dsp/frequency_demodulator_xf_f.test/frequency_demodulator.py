#!/usr/bin/env python3

# Python implementation of frequency demodulator for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of frequency demodulator."""

import numpy

import opencpi.ocpi_testing as ocpi_testing


class FrequencyDemodulator(ocpi_testing.Implementation):
    """Python implementation of frequency demodulator."""

    def __init__(self):
        """Initialise the class."""
        super().__init__()
        self.reset()

    def reset(self):
        """Reset the state to the same state as at initialisation."""
        self._previous_sample = complex(numpy.float32(0.0), numpy.float32(0.0))

    def sample(self, input_):
        """Handle an incoming sample opcode.

        Args:
            input_ (list of complex float): The sample values on the input_ port.

        Returns:
            list of float: Formatted messages.
        """
        output = [complex(0.0, 0.0)] * len(input_)
        for index, sample in enumerate(input_):
            output[index] = self._calculate_sample(sample)
            self._previous_sample = sample

        return self.output_formatter([{"opcode": "sample", "data": output}])

    def _calculate_sample(self, sample):
        current = numpy.complex64(sample)
        conjugate = numpy.complex64(complex(self._previous_sample.real,
                                            - self._previous_sample.imag))
        difference = current * conjugate

        return numpy.arctan2(difference.imag, difference.real)
