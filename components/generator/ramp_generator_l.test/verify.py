#!/usr/bin/env python3

# Runs checks for ramp_generator_l testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verification script for the ramp_generator_l testing."""

import os
import sys

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing

from ramp_generator import RampGenerator


output_file_path = str(sys.argv[-1])

enable_property = os.environ.get("OCPI_TEST_enable").upper() == "TRUE"
message_length_property = int(os.environ.get("OCPI_TEST_message_length"))
step_size_property = int(os.environ.get("OCPI_TEST_step_size"))
start_amplitude_property = int(os.environ.get("OCPI_TEST_start_amplitude"))
stop_amplitude_property = int(os.environ.get("OCPI_TEST_stop_amplitude"))
discontinuity_on_amplitude_change_property = os.environ.get(
    "OCPI_TEST_discontinuity_on_amplitude_change").upper() == "TRUE"
discontinuity_on_step_size_change_property = os.environ.get(
    "OCPI_TEST_discontinuity_on_step_size_change").upper() == "TRUE"

ramp_generator_implementation = RampGenerator(
    enable_property, message_length_property, step_size_property,
    start_amplitude_property, stop_amplitude_property,
    discontinuity_on_amplitude_change_property,
    discontinuity_on_step_size_change_property)

# Set the reference file length by determining the length of the
# output file generated by the implementation under test.
number_of_messages = 1024  # Limits the number of messages parsed to this many
with ocpi_protocols.ParseMessagesFile(
        output_file_path, "long_timed_sample",
        number_of_messages) as output_file:
    output_length = output_file.get_sample_data_length()
ramp_generator_implementation.no_input_settings[
    "total_output_length"] = output_length

test_id = ocpi_testing.get_test_case()

if enable_property and output_length == 0:
    print("There is no data within the output files")
    sys.exit(1)

# Check the result
verifier = ocpi_testing.Verifier(ramp_generator_implementation)
verifier.set_port_types([], ["long_timed_sample"], ["equal"])

if verifier.verify(test_id, [], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
