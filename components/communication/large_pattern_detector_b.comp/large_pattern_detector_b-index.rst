.. large_pattern_detector_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _large_pattern_detector_b:


Pattern Detector (Large) (``large_pattern_detector_b``)
=======================================================
Finds a set binary pattern in a boolean stream.

Design
------
The input boolean stream is checked to see if it contains a set pattern.

The mathematical representation of when a match with the set pattern is given in :eq:`large_pattern_detector_b-equation`.

.. math::
   :label: large_pattern_detector_b-equation

   result = (memory \texttt{ AND } bitmask) \texttt{ XOR } (pattern \texttt{ AND } bitmask)

In :eq:`large_pattern_detector_b-equation` the :math:`\texttt{AND}` and :math:`\texttt{XOR}` are bit-wise operations, with a match detected when :math:`result` takes all 0s.

 * :math:`memory` is the most recently received bits being searched for the pattern.

 * :math:`bitmask` selects which bits are used in the pattern match.

 * :math:`pattern` is the pattern being searched for in the boolean stream.

When a pattern is detected a metadata message is generated.

Interface
---------
.. literalinclude:: ../specs/large_pattern_detector_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode Handling
~~~~~~~~~~~~~~~
Sample messages are searched for the set pattern but otherwise passed directly from input to output without modification.

A metadata message is inserted at the moment a pattern match occurs. The ``value`` field of the metadata message is set by the ``metadata_value`` property, and the ``id`` field is set by the ``metadata_id`` property.

Discontinuity and flush messages are passed through the component, and reset the counter of bits received.  Thus a pattern match is not identified if the matching bits span a flush or discontinuity.

All other message types are passed through the component unmodified.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   bitmask: Can be used to match patterns shorter than ``pattern_length`` bits, or to ignore specific bits in a pattern. A value of 0 in the N-th bit of ``bitmask`` will exclude the N-th bit of ``pattern`` and memory from being compared. A value of 1 in the N-th bit of ``bitmask`` will include the N-th bit of ``pattern`` and memory in the comparison. When ``bitmask`` is set to all 0s, pattern matching is disabled.

Implementations
---------------
.. ocpi_documentation_implementations:: ../large_pattern_detector_b.hdl ../large_pattern_detector_b.rcc

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

* :ref:`Protocol interface delay primitive v2 <protocol_interface_delay_v2-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.math_real``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``large_pattern_detector_b`` are:

 * The HDL implementation is designed to be resource efficient and use BRAM, which results in more than a single clock cycle being required for each pattern match comparison.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
