-- VHDL implementation of large pattern detector worker.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Searches a boolean stream for a user-defined pattern.
-- Injects a metadata message if the pattern is detected.
-- Uses a user-defined bit mask to select which bits are compared.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library ocpi;
use ocpi.types.all;

library sdr_interface;
use sdr_interface.sdr_interface.metadata_inserter_v2;

architecture rtl of worker is

  constant pattern_length_c         : integer := to_integer(pattern_length);
  constant pattern_detect_latency_c : natural := 2;

  constant opcode_width_c : integer := integer(ceil(log2(real(
    bool_timed_sample_opcode_t'pos(bool_timed_sample_opcode_t'high)))));

  function opcode_to_slv(inop : in bool_timed_sample_opcode_t) return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(bool_timed_sample_opcode_t'pos(inop), opcode_width_c));
  end function;

  function slv_to_opcode(inslv : in std_logic_vector(opcode_width_c - 1 downto 0)) return bool_timed_sample_opcode_t is
  begin
    return bool_timed_sample_opcode_t'val(to_integer(unsigned(inslv)));
  end function;

  signal metadata_id    : std_logic_vector(props_in.metadata_id'range);
  signal metadata_value : std_logic_vector(props_in.metadata_value'range);
  signal match_found    : std_logic;
  signal input_take     : std_logic;

  signal message_complete : std_logic;

  signal input_opcode  : std_logic_vector(opcode_width_c - 1 downto 0);
  signal output_opcode : std_logic_vector(opcode_width_c - 1 downto 0);

  signal pattern_count : natural range 0 to pattern_length_c-1;
  signal pattern       : std_logic_vector(pattern_length_c-1 downto 0);
  signal bitmask       : std_logic_vector(pattern_length_c-1 downto 0);
  signal pattern_match : std_logic_vector(pattern_length_c-1 downto 0);

  signal props_data : std_logic;
  signal data_valid : std_logic;

  type worker_input_stream_delay_t is array (pattern_detect_latency_c -1 downto 0) of worker_input_in_t;
  signal worker_input_stream_delay : worker_input_stream_delay_t;

begin

  -- props data bit is extracted from the props bus using the byte enable flag
  -- to select the byte
  props_data <= props_in.raw.data(0) when props_in.raw.byte_enable(0) = '1' else
                props_in.raw.data(8)  when props_in.raw.byte_enable(1) = '1' else
                props_in.raw.data(16) when props_in.raw.byte_enable(2) = '1' else
                props_in.raw.data(24) when props_in.raw.byte_enable(3) = '1' else
                '0';

  data_valid <= '1' when (output_in.ready = '1') and
                (input_take = '1') and
                (input_in.valid = '1' or input_in.ready = '1') else '0';

  -- props_out set to default
  props_out.raw.data  <= (others => '0');
  props_out.raw.done  <= btrue;
  props_out.raw.error <= bfalse;

  delay_input_steam_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then  -- No reset required as happens before code starts
      if input_take = '1' then
        worker_input_stream_delay <= worker_input_stream_delay(pattern_detect_latency_c - 2 downto 0) & input_in;
      end if;
    end if;
  end process delay_input_steam_p;

  -----------------------------------------------------------------------------
  -- Reads in the pattern data and bitmask data from the property data
  -----------------------------------------------------------------------------
  props_data_proc_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then  -- No reset required as happens before code starts
      -- Assumption is that this will be a once only occurance
      -- i.e only one pattern will be searched for at a time.
      -- This will not change during normal run time.
      if props_in.raw.is_write = '1' then
        if(to_integer(unsigned(props_in.raw.address)) <= pattern_length_c-1) then
          pattern <= pattern(pattern_length_c-2 downto 0) & props_data;
        else
          bitmask <= bitmask(pattern_length_c-2 downto 0) & props_data;
        end if;

      end if;

      -- Register the metadata id and value when it is updated
      if props_in.metadata_value_written = '1' then
        metadata_value <= std_logic_vector(props_in.metadata_value);
      end if;
      if props_in.metadata_id_written = '1' then
        metadata_id <= std_logic_vector(props_in.metadata_id);
      end if;

    end if;
  end process props_data_proc_p;

  -----------------------------------------------------------------------------
  --  Update the pattern buffer and count how many samples are stored
  -----------------------------------------------------------------------------
  buffer_data_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then

      if ctl_in.reset = '1' then
        pattern_count <= 0;

      elsif data_valid = '1' then
        -- Reset the pattern buffer and trigger on flush or discontinuity
        if input_in.opcode = bool_timed_sample_flush_op_e or input_in.opcode = bool_timed_sample_discontinuity_op_e then
          pattern_count <= 0;
        -- Check for matched pattern and set the metadata trigger
        elsif input_in.opcode = bool_timed_sample_sample_op_e then
          if pattern_count < pattern_length_c-1 then
            pattern_count <= pattern_count + 1;
          end if;
          pattern_match <= pattern_match(pattern_length_c-2 downto 0) & input_in.data(0);
        end if;
      end if;
    end if;
  end process buffer_data_p;

  -----------------------------------------------------------------------------
  --  Detect matches
  -----------------------------------------------------------------------------
  match_found_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if output_in.ready = '1' then
        match_found <= '0';
        if (bitmask and pattern) = (pattern_match and bitmask) and pattern_count = pattern_length_c-1 then
          match_found <= '1';
        end if;
      end if;
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- Instantiate the metadata primitive to do the work of sending the message
  -----------------------------------------------------------------------------
  metadata_inserter_i : metadata_inserter_v2
    generic map (
      data_width_g        => input_in.data'length,
      opcode_width_g      => opcode_width_c,
      byte_enable_width_g => input_in.byte_enable'length,
      metadata_opcode_g   => opcode_to_slv(bool_timed_sample_metadata_op_e),
      data_opcode_g       => opcode_to_slv(bool_timed_sample_sample_op_e)
      ) port map (
        clk                => ctl_in.clk,
        reset              => ctl_in.reset,
        metadata_id        => metadata_id,
        metadata_value     => metadata_value,
        trigger            => match_found,
        message_complete   => message_complete,
        take_in            => output_in.ready,
        take_out           => input_take,
        input_data         => worker_input_stream_delay(0).data,
        input_byte_enable  => worker_input_stream_delay(0).byte_enable,
        input_opcode       => input_opcode,
        input_som          => worker_input_stream_delay(0).som,
        input_eom          => worker_input_stream_delay(0).eom,
        input_eof          => worker_input_stream_delay(0).eof,
        input_valid        => worker_input_stream_delay(0).valid,
        input_ready        => worker_input_stream_delay(0).ready,
        output_data        => output_out.data,
        output_byte_enable => output_out.byte_enable,
        output_opcode      => output_opcode,
        output_eom         => output_out.eom,
        output_som         => output_out.som,
        output_eof         => output_out.eof,
        output_valid       => output_out.valid,
        output_give        => output_out.give
        );
  input_out.take <= input_take;

  -- Convert the opcodes to/from the enumerated type
  output_out.opcode <= slv_to_opcode(output_opcode);
  input_opcode      <= opcode_to_slv(worker_input_stream_delay(0).opcode);

end rtl;
