#!/usr/bin/env python3

# Runs on correct output for bit_stuffer_b implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Verification script for the bit_stuffer_b implementation."""

import os
import sys

import opencpi.ocpi_testing as ocpi_testing

from bit_stuffer import BitStuffer


consecutive_ones_property = int(os.environ.get("OCPI_TEST_consecutive_ones"))
flush_length_property = int(os.environ.get("OCPI_TEST_flush_length"))

input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

bit_stuffer_implementation = BitStuffer(consecutive_ones_property,
                                        flush_length_property)

test_id = ocpi_testing.get_test_case()

verifier = ocpi_testing.Verifier(bit_stuffer_implementation)
verifier.set_port_types(
    ["bool_timed_sample"], ["bool_timed_sample"], ["equal"])
if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
