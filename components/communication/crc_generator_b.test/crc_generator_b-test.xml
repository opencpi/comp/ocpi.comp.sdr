<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property test="true" name="subcase" type="string"/>
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
  </case>
  <!-- Bug in OpenCPI 1.4 which means multiple property values cannot be tested
       when the property is an array; this limits polynomial_taps testing. -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps"
      value="64,62,57,55,54,53,52,47,46,45,40,39,38,37,35,33,32,31,29,27,24,23,22,21,19,17,13,12,10,9,7,4,1"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" value="polynomial_taps"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" values="false,true"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" value="output_reflect"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" values="0x0000,0xffff,0x5555,0x99ff,0x13ca"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" value="seed"/>
  </case>
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" values="0x0000,0xffff,0x5555,0x6d23,0x72ae"/>
    <property name="subcase" value="final_xor"/>
  </case>
  <!-- Cannot currently verify as no way to determine when property changed in
       relation to data. -->
  <!--
  <case>
    <input port="input" script="generate.py -case property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect">
      <set delay="0" value="false"/>
      <set delay="1" value="true"/>
    </property>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" value="output_reflect"/>
  </case>
  <case>
    <input port="input" script="generate.py -case property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed">
      <set delay="0" value="0"/>
      <set delay="1" value="0xFFFF"/>
    </property>
    <property name="final_xor" value="0"/>
    <property name="subcase" value="seed"/>
  </case>
  <case>
    <input port="input" script="generate.py -case property_change" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor">
      <set delay="0" value="0"/>
      <set delay="1" value="0xFFFF"/>
    </property>
    <property name="subcase" value="final_xor"/>
  </case>
  -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="all_zero,all_maximum"/>
  </case>
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
  </case>
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="16,15,2"/>
    <property name="output_reflect" value="false"/>
    <property name="seed" value="0"/>
    <property name="final_xor" value="0"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="polynomial_taps" value="32,26,23,22,16,12,11,10,8,7,5,4,2,1"/>
    <property name="output_reflect" values="false,true"/>
    <property name="seed" values="0x50d7caab,0xc155a72a"/>
    <property name="final_xor" values="0x1cbc2510,0x171a344e"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
