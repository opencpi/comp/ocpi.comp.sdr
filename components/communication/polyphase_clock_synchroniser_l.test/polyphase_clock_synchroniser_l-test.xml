<?xml version="1.0"?>
<!-- This file is protected by Copyright. Please refer to the COPYRIGHT file
     distributed with this source distribution.

     This file is part of OpenCPI <http://www.opencpi.org>

     OpenCPI is free software: you can redistribute it and/or modify it under
     the terms of the GNU Lesser General Public License as published by the Free
     Software Foundation, either version 3 of the License, or (at your option)
     any later version.

     OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
     more details.

     You should have received a copy of the GNU Lesser General Public License
     along with this program. If not, see <http://www.gnu.org/licenses/>. -->
<tests usehdlfileio="true">
  <property name="subcase" test="true" type="string"/>
  <property name="phase_offset" test="true" type="float" value="0.58905"/>
  <property name="frequency_offset" test="true" type="float" value="1e-6"/>
  <property name="awgn_level" test="true" type="short" value="-8"/>
  <!-- Case 00 Typical -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 01 Rounding Type -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" values="half_up,half_even,truncate"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 02 Max Taps per Branch -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="256"/>
    <property name="interpolation_factor" value="4"/>
    <property name="number_of_taps" value="256"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 03 Max Interpolation  -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_up"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="16"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 04 Number of Taps  -->
  <case>
    <input port="input" script="generate.py --case property" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" values="48,64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 05 Loop Coefficient -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 06 Phase Offsets -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" values="-0.392699,0.785398,0.0,0.392699,2.243995"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 07 Frequency Offset  -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" values="0.0, -15e-6, 50e-6, 10e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 08 Build Variable Tests -->
  <case>
    <input port="input" script="generate.py --case typical" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" values="half_up,truncate"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" values="8,16"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 09 Test Full scale lock  -->
  <case>
    <input port="input" script="generate.py --case sample" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="large_positive,large_negative,all_zero"/>
  </case>
  <!-- Case 10 Input Stressing-->
  <case>
    <input port="input" script="generate.py --case input_stressing" messagesinfile="true" messagesize="16384" stressormode="full"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
  </case>
  <!-- Case 11 Message Lengths -->
  <case>
    <input port="input" script="generate.py --case message_size" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true" messagesize="16384"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="shortest,longest,different_sizes"/>
  </case>
  <!-- Case 12 Time opcodes -->
  <case>
    <input port="input" script="generate.py --case time" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 13 Sample Interval opcode -->
  <case>
    <input port="input" script="generate.py --case sample_interval" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 14 Flush condition -->
  <case>
    <input port="input" script="generate.py --case flush" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 15 Discontinuity opcode -->
  <case>
    <input port="input" script="generate.py --case discontinuity" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="single,consecutive"/>
  </case>
  <!-- Case 16 metadata -->
  <case>
    <input port="input" script="generate.py --case metadata" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="zero,positive,maximum,consecutive"/>
  </case>
  <!-- Case 17 Soak -->
  <case>
    <input port="input" script="generate.py --case soak" messagesinfile="true" messagesize="16384"/>
    <output port="output" script="verify.py" messagesinfile="true"/>
    <property name="rounding_type" value="half_even"/>
    <property name="max_number_of_taps" value="64"/>
    <property name="interpolation_factor" value="8"/>
    <property name="number_of_taps" value="64"/>
    <property name="taps" generate="generate_taps.py long 31"/>
    <!-- Loop coefficient are "1.5,0.05,0.5" but scaled by 2^26=67108864.-->
    <property name="loop_coefficients" value="100663296,3355443,33554432"/>
    <property name="phase_offset" value="0.58905"/>
    <property name="frequency_offset" value="1e-6"/>
    <property name="awgn_level" value="-8"/>
    <property name="subcase" values="sample_only,all_opcodes"/>
  </case>
</tests>
