#!/usr/bin/env python3

# Generates the input binary file for crc_generator_c testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Generates the input binary file for crc_generator testing."""

import os

import opencpi.ocpi_protocols as ocpi_protocols
import opencpi.ocpi_testing as ocpi_testing


class CrcGenerator(ocpi_testing.generator.CharacterGenerator):
    """Custom generator for CRC testing."""

    def typical(self, seed, subcase):
        """Generate a sample message with typical data inputs.

        Typical behaviour for CRC Generator is to expect a flush or
        discontinuity after streamed data in order to trigger a CRC
        to be generated, so override the default typical behaviour for
        this particular case.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the typical case and the stated subcase.
        """
        messages = super().typical(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages

    def property(self, seed, subcase):
        """Generate sample message to allow property value testing.

        Typical behaviour for CRC Generator is to expect a flush or
        discontinuity after streamed data in order to trigger a CRC
        to be generated, so override the default typical behaviour for
        this particular case.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the property testing.
        """
        messages = super().property(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages

    def sample(self, seed, subcase):
        """Generate sample messages to test different supported values.

        Typical behaviour for CRC Generator is to expect a flush or
        discontinuity after streamed data in order to trigger a CRC
        to be generated, so override the default typical behaviour for
        this particular case.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the sample case and the stated subcase.
        """
        messages = super().sample(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages

    def message_size(self, seed, subcase):
        """Generate sample messages to test handling of different message sizes.

        Typical behaviour for CRC Generator is to expect a flush or
        discontinuity after streamed data in order to trigger a CRC
        to be generated, so override the default typical behaviour for
        this particular case.

        Args:
            seed (int): The seed value to use for random number generation.
            subcase (str): Name of the subcase messages are to be generated for.

        Returns:
            Messages for the message_size case and the stated subcase.
        """
        messages = super().message_size(seed, subcase)
        messages += ([{"opcode": "discontinuity", "data": None}])
        return messages


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
polynomial_taps_property = os.environ.get("OCPI_TEST_polynomial_taps")
input_reflect_property = (
    os.environ.get("OCPI_TEST_input_reflect").lower() == "true")
output_reflect_property = (
    os.environ.get("OCPI_TEST_output_reflect").lower() == "true")
seed_property = int(os.environ.get("OCPI_TEST_seed"))
final_xor_property = int(os.environ.get("OCPI_TEST_final_xor"))

seed = ocpi_testing.get_test_seed(arguments.case, subcase,
                                  polynomial_taps_property,
                                  input_reflect_property,
                                  output_reflect_property, seed_property,
                                  final_xor_property)

generator = CrcGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path,
                                      "char_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
