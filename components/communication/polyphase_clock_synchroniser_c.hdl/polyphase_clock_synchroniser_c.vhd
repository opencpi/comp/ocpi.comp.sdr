-- polyphase_clock_synchroniser_c HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;

library sdr_dsp;
use sdr_dsp.sdr_dsp.polyphase_interpolator;

library sdr_communication;
use sdr_communication.sdr_communication.gardner_timing_error_detector;

architecture rtl of worker is
  constant input_width_c    : integer := input_in.data'length;
  constant tap_addr_width_c : integer :=
    integer(ceil(log2(real(to_integer(max_number_of_taps)))));
  constant output_delay_c        : integer                     := 5 + to_integer(max_number_of_taps);
  constant input_max_c           : signed(input_in.data'range) := '0' & to_signed(-1, input_in.data'length-1);
  constant interpolation_width_c : integer                     := integer(ceil(log2(real(to_integer(interpolation_factor)))));
  constant flush_length_c        : integer                     := to_integer(max_number_of_taps) / to_integer(interpolation_factor);

  type clock_sync_state_t is (idle_s, stuff_s, interp_s);

  signal worker_state      : clock_sync_state_t;
  signal input_in_internal : worker_input_in_t;

  signal reset            : std_logic;
  signal take_in          : std_logic;
  signal take_out         : std_logic;
  signal input_data_valid : std_logic;
  signal input_data_ready : std_logic;
  signal output_ready     : std_logic;

  signal other_opcode_valid       : std_logic;
  signal interpolator_input_valid : std_logic;
  signal interpolator_single      : std_logic;
  signal interpolator_input_ready : std_logic;

  signal skip              : std_logic;
  signal stuff             : std_logic;
  signal stuff_r           : std_logic;
  signal output_valid      : std_logic;
  signal processed_mask_in : std_logic;

  signal data_in_buffer      : std_logic;
  signal branch              : unsigned(interpolation_width_c-1 downto 0);
  signal branch_r            : unsigned(interpolation_width_c-1 downto 0);
  signal interpolator_branch : unsigned(tap_addr_width_c-1 downto 0) := (others => '0');
  signal tap_addr            : unsigned(tap_addr_width_c - 1 downto 0);
  signal tap_data            : signed(input_width_c-1 downto 0);
  signal tap_write           : std_logic;

  signal interpolator_data_out  : signed(input_width_c-1 downto 0);
  signal ted_data_in            : signed(input_width_c-1 downto 0);
  signal interpolator_valid_out : std_logic;
  signal interpolator_last_out  : std_logic;
  signal processed_last_in      : std_logic;
  signal processed_last_in_r    : std_logic;

  signal ted_input_valid : std_logic;
  signal ted_valid       : std_logic;
  signal ted_valid_r     : std_logic_vector(1 downto 0);
  signal ted_data        : std_logic_vector(input_width_c-1 downto 0);

  signal integral_error     : signed(2*input_width_c - 1 downto 0);
  signal proportional_error : signed(2*input_width_c - 1 downto 0);
  signal total_error        : signed(2*input_width_c - 1 downto 0);
  signal accumulator        : signed(2*input_width_c - 1 downto 0);

  signal downsampler_mask : std_logic;

  signal number_of_taps : unsigned(tap_addr_width_c-1 downto 0);
  signal flush_length   : unsigned(tap_addr_width_c-1 downto 0);

  function rounding_type_to_string(rounding_type : in work.polyphase_clock_synchroniser_c_constants.rounding_type_t)
    return string is
  begin
    if rounding_type = truncate_e then
      return "truncate";
    elsif rounding_type = half_up_e then
      return "half_up";
    elsif rounding_type = half_even_e then
      return "half_even";
    end if;
  end function;

begin

  flush_i : entity work.char_flush_injector
    generic map (
      data_in_width_g => input_in.data'length
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take_in,
      input_in        => input_in,
      flush_length    => flush_length,
      input_out       => input_out,
      input_interface => input_in_internal
      );

  -- Input take in signal
  take_in      <= output_ready and (input_data_ready or other_opcode_valid);
  -- Output take in signal (with extra flag for when stuffing samples)
  take_out     <= take_in or interpolator_single;
  -- The output is ready and the protocol delay is not busy inserting a message
  output_ready <= output_in.ready;

  -- This is a data sample
  input_data_valid <= '1' when (input_in_internal.opcode = char_timed_sample_sample_op_e
                                and input_in_internal.valid = '1')
                      else '0';

  -- This is not a data sample, and the core is in a state to allow
  -- processing of this message
  other_opcode_valid <= '1' when (input_in_internal.opcode /= char_timed_sample_sample_op_e
                                  and input_in_internal.ready = '1' and worker_state = idle_s)
                        else '0';

  -- reset on discontinuity or global reset
  reset <= '1' when ctl_in.reset = '1' or (input_in_internal.opcode = char_timed_sample_discontinuity_op_e
                                           and input_in_internal.ready = '1' and take_in = '1')
           else '0';

  -- Tap data is only 16 bits, therefore there are 2 tap messages possible
  -- on the raw interface
  tap_data <= signed(props_in.raw.data(7 downto 0)) when props_in.raw.byte_enable(0) = '1'
              else signed(props_in.raw.data(23 downto 16));

  -- Plus one offset as byte addressing is being converted to short
  -- (even though the data is bytes)
  tap_addr <= props_in.raw.address(tap_addr'high + 1 downto 1);

  -- only flush when there has been some data processed
  -- (in case of double flush)
  flush_length <= to_unsigned(flush_length_c - 1, flush_length'length) when data_in_buffer = '1' else (others => '0');

  -- Get the current number of taps expected per branch (value passed onto
  -- interpolator, to protect reading unset data in tap ram)
  -- The value is provided as length of taps, therefore -1 as the
  -- interpolator counts 0-(N-1) not 1-N
  number_of_taps <= resize(props_in.number_of_taps, number_of_taps'length);

  input_state_machine_p : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if reset = '1' then
        worker_state             <= idle_s;
        data_in_buffer           <= '0';
        input_data_ready         <= '0';
        branch_r                 <= (others => '0');
        interpolator_input_valid <= '0';
        interpolator_single      <= '0';
        stuff_r                  <= '0';
        ted_input_valid          <= '0';
        output_valid             <= '0';

      elsif output_ready = '1' then
        input_data_ready         <= '0';
        interpolator_input_valid <= '0';
        interpolator_single      <= '0';
        ted_input_valid          <= '0';
        output_valid             <= '0';

        case worker_state is
          when idle_s =>
            stuff_r <= '0';
            if input_data_valid = '1' and interpolator_input_ready = '1' then
              interpolator_input_valid <= '1';
              input_data_ready         <= '1';
              worker_state             <= interp_s;
              branch_r                 <= branch;

              -- Is this a flush, or data? (Other opcodes do not affect sample
              -- data), therefore don't change the data buffer state
              if input_in.ready = '1' and input_in.opcode = char_timed_sample_flush_op_e then
                data_in_buffer <= '0';
              else
                data_in_buffer <= '1';
              end if;
            end if;

          when interp_s =>
            if interpolator_valid_out = '1' then
              -- We don't output on a skip, (we only add to the sample buffer)
              if skip = '1' then
                worker_state <= idle_s;
                branch_r     <= branch;

              -- When stuffing we add one sample, then interp it twice
              -- with different branch settings, and pass both of these
              -- to the output
              elsif stuff = '1' or stuff_r = '1' then
                worker_state <= stuff_s;
                branch_r     <= branch;

                interpolator_single <= '1';
                output_valid        <= '1';
                ted_data_in         <= interpolator_data_out;

              -- Otherwise we return to a waiting state for the next sample
              -- And mark this as an output sample
              else
                worker_state    <= idle_s;
                output_valid    <= '1';
                ted_input_valid <= '1';
                ted_data_in     <= interpolator_data_out;
              end if;
            end if;

          when stuff_s =>
            if interpolator_valid_out = '1' then
              worker_state <= idle_s;
              output_valid <= '1';
              if abs(ted_data_in) < abs(interpolator_data_out) then
                ted_data_in <= interpolator_data_out;
              end if;
            end if;

        end case;
      end if;
    end if;
  end process;

  branch              <= unsigned(accumulator(accumulator'high downto accumulator'length-interpolation_width_c));
  total_error         <= integral_error + proportional_error;
  interpolator_branch <= resize(branch_r, tap_addr_width_c);
  -- The output of the Timing Error Detector (TED) is proportional to the
  -- distance from zero crossing, therefore we treat like a PLL phase error
  -- and use a Proportional Interpolator (PI) filter to correct for this.
  -- The use of stuff and skip are required when we reach the ends of the
  -- interpolator, and so change the center symbol.
  -- Stuff: Add an extra sample so that the center symbol moved backwards
  -- Skip: No sample is added, so the center symbol moves forward
  loop_filter : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if reset = '1' then
        ted_valid_r        <= (others => '0');
        integral_error     <= (others => '0');
        proportional_error <= (others => '0');
        accumulator        <= (others => '0');
        stuff              <= '0';
        skip               <= '0';
      else
        -- Clear any skip or stuff flags from previous sample.
        if interpolator_input_valid = '1' or interpolator_single = '1' then
          skip  <= '0';
          stuff <= '0';
        end if;

        if ted_valid = '1' then
          -- Errors are 2S10.20
          integral_error     <= signed(ted_data) * resize(signed(props_in.loop_coefficients(1)), input_width_c) + integral_error;
          proportional_error <= signed(ted_data) * resize(signed(props_in.loop_coefficients(0)), input_width_c);
        end if;
        if ted_valid_r(0) = '1' then
          -- Errors are 2S10.20, therefore move back down to S5.10
          accumulator <= (resize(shift_right(total_error, input_width_c), input_width_c) * resize(signed(props_in.loop_coefficients(2)), input_width_c))
                         + accumulator;
        end if;
        if ted_valid_r(1) = '1' then
          if branch = 0 and branch_r = to_integer(interpolation_factor)-1 then
            skip <= '1';
          elsif branch_r = 0 and branch >= to_integer(interpolation_factor)-1 then
            stuff <= '1';
          end if;
        end if;
      end if;
      ted_valid_r(0) <= ted_valid;
      ted_valid_r(1) <= ted_valid_r(0);
    end if;
  end process loop_filter;

  tap_write <= '1' when props_in.raw.is_write = '1' else '0';

  interpolator_i : polyphase_interpolator
    generic map (
      complex_data_g  => false,
      single_branch_g => true,
      data_width_g    => input_width_c,
      max_num_taps_g  => to_integer(max_number_of_taps),
      rounding_mode_g => rounding_type_to_string(rounding_type)
      )
    port map (
      clk                  => ctl_in.clk,
      reset                => reset,
      enable               => output_ready,
      interpolation_factor => resize(interpolation_factor, tap_addr_width_c),
      num_taps_in          => number_of_taps,
      run_single_in        => interpolator_single,
      branch_in            => interpolator_branch,

      tap_data_in   => tap_data,
      tap_addr_in   => tap_addr,
      tap_valid_in  => tap_write,
      tap_ready_out => props_out.raw.done,

      input_data_i_in   => signed(input_in_internal.data(7 downto 0)),
      input_data_q_in   => (others => '-'),
      input_valid_in    => interpolator_input_valid,
      input_last_in     => input_in_internal.eom,
      input_ready_out   => interpolator_input_ready,
      output_data_i_out => interpolator_data_out,
      output_data_q_out => open,
      output_valid_out  => interpolator_valid_out,
      output_last_out   => interpolator_last_out,
      output_ready_in   => output_ready
      );

  timing_error_detector_i : gardner_timing_error_detector
    generic map (
      data_width_g => input_width_c
      )
    port map (
      clk          => ctl_in.clk,
      reset        => reset,
      resync       => '0',
      input_valid  => ted_input_valid,
      input_data   => std_logic_vector(ted_data_in),
      output_valid => ted_valid,
      output_data  => ted_data
      );

  downsampler : process (ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if reset = '1' then
        downsampler_mask <= '0';
      elsif output_valid = '1' then
        downsampler_mask <= not downsampler_mask;
      end if;

      if processed_mask_in = '1' then
        processed_last_in_r <= '0';
      elsif downsampler_mask = '1' and input_in_internal.eom = '1' and input_data_valid = '1' then
        processed_last_in_r <= '1';
      end if;
    end if;
  end process downsampler;
  processed_last_in <= processed_last_in_r or (downsampler_mask and input_in_internal.eom and input_data_valid);
  processed_mask_in <= downsampler_mask and output_valid;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the downsample calculation.
  interface_delay_i : entity work.char_down_sampled_protocol_delay
    generic map (
      data_in_width_g => input_in_internal.data'length
      )
    port map (
      clk                       => ctl_in.clk,
      reset                     => ctl_in.reset,
      enable                    => output_in.ready,
      take_in                   => take_out,
      group_delay_seconds_in    => (others => '0'),
      group_delay_fractional_in => (others => '0'),

      input_in            => input_in_internal,
      processed_stream_in => std_logic_vector(ted_data_in),
      processed_valid_in  => processed_mask_in,
      processed_last_in   => processed_last_in,
      output_out          => output_out
      );

end rtl;
