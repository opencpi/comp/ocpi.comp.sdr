#!/usr/bin/env python3

# Python implementation of data_pack
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Python implementation of data_pack."""

import decimal

import numpy as np

import opencpi.ocpi_testing as ocpi_testing


class DataPack(ocpi_testing.Implementation):
    """Python implementation of data_pack."""

    def __init__(self, msb_first, flush_enable):
        """Initialise the class.

        Args:
            msb_first (bool): When true the output byte is packed starting with
                              the most significant bit first. When false the
                              output byte is packed starting with the least
                              significant bit first.
            flush_enable (bool): When true any non-sample opcode (except time)
                                 will cause any unsent data to be output as an
                                 incomplete data word before outputting the
                                 non-sample opcode. When false any non-sample
                                 opcode (except time or flush) will cause any
                                 unsent data to be discarded before outputting
                                 the non-sample opcode.
        """
        super().__init__(
            msb_first=msb_first, flush_enable=flush_enable)
        self._pack_buffer = []
        self._sample_interval = decimal.Decimal(0)
        self._timestamp = decimal.Decimal(0)
        self._send_timestamp = False

    def reset(self):
        """Put the implementation into the same state as at initialisation."""
        self._pack_buffer = []

    def sample(self, values):
        """Handle an incoming sample opcode message.

        Args:
            values (list): Incoming samples on the input port.

        Returns:
            Formatted messages.
        """
        # Append to existing buffer
        self._pack_buffer = self._pack_buffer + list(values)

        # Check if output will fit perfectly into bytes
        input_len_remainder = (len(self._pack_buffer) % 8)
        if input_len_remainder == 0:
            byte_inputs = bytearray(self._pack_buffer)
            self._pack_buffer = []
        else:
            byte_inputs = bytearray(
                self._pack_buffer[
                    :len(self._pack_buffer) - input_len_remainder
                ])
            # Store bits that have not been output
            self._pack_buffer = self._pack_buffer[
                len(self._pack_buffer) - input_len_remainder:]
        if self.msb_first:
            input_data_bits = np.packbits(byte_inputs)
        else:
            input_data_bits = np.packbits(byte_inputs)
            input_data_bits = np.unpackbits(
                input_data_bits).reshape(-1, 8)[:, ::-1].ravel()
            input_data_bits = np.packbits(input_data_bits)

        output = list(input_data_bits)

        if self._send_timestamp and (len(output) > 0):
            # Send remainder of data word
            messages = [{"opcode": "sample", "data": [output[0]]}]
            # Add in timestamp
            messages.append({"opcode": "time", "data": self._timestamp})
            # Send rest of data words
            output.pop(0)
            messages += [{"opcode": "sample", "data": [output_bit]}
                         for output_bit in output]
            # Clear the timestamp flag
            self._send_timestamp = False
            return self.output_formatter(messages)
        else:
            return self.output_formatter(
                [{"opcode": "sample", "data": [output_bit]}
                 for output_bit in output])

    def flush_buffer(self, next_opcode):
        """Flush out the internal buffer and append the next opcode.

        Args:
            next_opcode (str): The opcode to output after flushing the buffer.

        Returns:
            List: Opcodes to be output.
        """
        bits_in_buffer = len(self._pack_buffer)
        if bits_in_buffer > 0:
            flush_data = [0] * (8 - bits_in_buffer)
            result = self.sample(flush_data).output
            self._send_timestamp = False
            return result + [next_opcode]
        else:
            return [next_opcode]

    def clear_buffer(self, next_opcode):
        """Clear the internal buffer and return the next opcode.

        Args:
            next_opcode (str): THe opcode to output after clearing the buffer.

        Returns:
            List: Opcodes to be output.
        """
        self._pack_buffer = []
        self._send_timestamp = False
        return [next_opcode]

    def non_stream(self, opcode):
        """Empty the internal buffer and output the next opcode.

        Args:
            opcode (str): The opcode to output after the buffer.

        Returns:
            List: Opcodes to be output.
        """
        if self.flush_enable:
            return self.flush_buffer(opcode)
        else:
            return self.clear_buffer(opcode)

    def flush(self, *inputs):
        """Handle an incoming flush opcode message.

        Args:
            inputs: Ignored.

        Returns:
            Formatted messages.
        """
        return self.output_formatter(
            self.flush_buffer({"opcode": "flush", "data": None}))

    def discontinuity(self, *inputs):
        """Handle an incoming discontinuity opcode message.

        Args:
            inputs: Ignored.

        Returns:
            Formatted messages.
        """
        return self.output_formatter(
            self.clear_buffer({"opcode": "discontinuity", "data": None}))

    def sample_interval(self, value):
        """Handle an incoming sample interval opcode message.

        Args:
            value (decimal): The sample interval.

        Returns:
            Formatted messages.
        """
        self._sample_interval = value
        return self.output_formatter(
            self.non_stream({"opcode": "sample_interval", "data": value}))

    def metadata(self, value):
        """Handle an incoming metadata opcode message.

        Args:
            value (tuple): The metadata id/value pair.

        Returns:
            Formatted messages.
        """
        return self.output_formatter(
            self.non_stream({"opcode": "metadata", "data": value}))

    def time(self, value):
        """Handle an incoming time opcode message.

        Args:
            value (decimal): The time.

        Returns:
            Formatted messages.
        """
        if len(self._pack_buffer) == 0:
            return self.output_formatter([{"opcode": "time", "data": value}])
        else:
            self._timestamp = decimal.Decimal((8 - len(self._pack_buffer))
                                              * self._sample_interval) + value
            self._send_timestamp = True
            return self.output_formatter([])
