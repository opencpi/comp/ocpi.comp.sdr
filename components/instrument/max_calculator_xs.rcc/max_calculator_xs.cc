// max_calculator_xs RCC worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "max_calculator_xs-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Max_calculator_xsWorkerTypes;

class Max_calculator_xsWorker : public Max_calculator_xsWorkerBase {
 private:
  static const int16_t max_reset_value{std::numeric_limits<int16_t>::min()};
  RCCBoolean max_i_is_valid{false};
  RCCBoolean max_q_is_valid{false};
  int16_t max_i{max_reset_value};
  int16_t max_q{max_reset_value};

  // Notification that max_i_is_valid property will be read
  RCCResult max_i_is_valid_read() {
    properties().max_i_is_valid = max_i_is_valid;
    return RCC_OK;
  }

  // Notification that max_q_is_valid property will be read
  RCCResult max_q_is_valid_read() {
    properties().max_q_is_valid = max_q_is_valid;
    return RCC_OK;
  }

  // Notification that max_i property will be read
  RCCResult max_i_read() {
    properties().max_i = this->max_i;
    // Reset value and valid flag
    this->max_i = max_reset_value;
    this->max_i_is_valid = false;
    return RCC_OK;
  }

  // Notification that max_q property will be read
  RCCResult max_q_read() {
    properties().max_q = this->max_q;
    // Reset value and valid flag
    this->max_q = max_reset_value;
    this->max_q_is_valid = false;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Complex_short_timed_sampleSample_OPERATION) {
      const auto* input_data = input.sample().data().data();
      const size_t input_size = input.sample().data().size();
      auto* output_data = output.sample().data().data();

      output.sample().data().resize(input_size);

      for (size_t i = 0; i < input_size; ++i, ++input_data) {
        if (input_data->real > this->max_i) {
          this->max_i = input_data->real;
        }

        if (input_data->imaginary > this->max_q) {
          this->max_q = input_data->imaginary;
        }

        output_data->real = input_data->real;
        output_data->imaginary = input_data->imaginary;
        ++output_data;
      }
      // Maximum values are valid if any samples have been received
      this->max_i_is_valid = true;
      this->max_q_is_valid = true;
      return RCC_ADVANCE;
    }

    // For all other opcodes just copy to output
    const size_t length = input.length();
    output.setLength(length);
    output.setOpCode(input.opCode());
    if (length > 0) {
      std::copy(input.data(), input.data() + length, output.data());
    }
    return RCC_ADVANCE;
  }
};

MAX_CALCULATOR_XS_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
MAX_CALCULATOR_XS_END_INFO
