#!/bin/bash
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License along
# with this program. If not, see <http://www.gnu.org/licenses/>.

[ -z "$OCPI_CDK_DIR" ] && echo 'Environment variable OCPI_CDK_DIR not set' && exit 1

target_component="$1"
target_platform="$2"
version=131.1.0  # latest as of 19/01/2022
name="kissfft"
description='KISS FFT Core source'
github_dl_url="https://github.com/mborgerding/kissfft/archive/refs/tags/"
dl_url="${KISS_FFT_URL:=${github_dl_url}}${version}.tar.gz"
cross_build=1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
fft_core_dest="$SCRIPT_DIR/../../components/dsp/${target_component:=fast_fourier_transform_xs.rcc}/gen/${name}"

destination="${name}-${version}"
# Check if destination folder already exists
if [[ -d "${fft_core_dest}" ]]; then
  echo "${description} prerequisite already installed on system"
  exit 0
fi

if [[ ! -d "$SCRIPT_DIR/../../components/dsp/${target_component}" ]]; then
  echo "Component ${target_component} does not exist"
  exit 0
fi

# Check if tarball already downloaded and extracted
cache_dir="$OCPI_CDK_DIR/../prerequisites-build"
fft_parent_dir="${cache_dir}/${name}/"
fft_extracted_dir="${cache_dir}/${name}/${destination}"

if [[ -d "${fft_extracted_dir}" ]]; then
  echo "${description} prerequisite already downloaded and extracted."

elif [[ -f "${cache_dir}/${destination}.tar.gz" ]]; then
  echo "${description} prerequisite already downloaded, extracting it."
  mkdir -p "${fft_parent_dir}"
  tar -C "${fft_parent_dir}" -xzf "${cache_dir}/${destination}.tar.gz"

else
  echo "Downloading and extracting ${description} prerequisite."

  # Download and extract source
  if ! (
    source "$OCPI_CDK_DIR/scripts/setup-prerequisite.sh" \
           "$target_platform" \
           "$name" \
           "$description" \
           "${dl_url%/*}" \
           "${dl_url##*/}" \
           "$destination" \
           "$cross_build"
  )
  then
    echo >&2 "WARNING: Failed to obtain ${description} prerequisite."
    echo >&2 "         This may need obtaining later to build the FFT"
    echo >&2 "         component - but can be ignored if not actually"
    echo >&2 "         building the FFT component."
    exit 0
  fi

  echo "Ready to copy extracted ${description} prerequisite code."
fi

# The part of the Kiss FFT core that is being used within the
# FFT RCC worker does not require installation or any additional
# building at this stage. Copy the contents of the extracted tar file
# to the fft_core_dest location.
if [[ -d "${fft_extracted_dir}" ]]; then
  echo "Linking extracted FFT source code"
  mkdir -p "$(dirname ${fft_core_dest})"
  ln -sf "${fft_extracted_dir}" "${fft_core_dest}"

else
  echo "Failed to copy FFT core, ${fft_extracted_dir} does not exist."
  exit 1
fi
