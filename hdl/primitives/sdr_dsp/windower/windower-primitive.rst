.. windower documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _windower-primitive:


Windower (``windower``)
=======================
A windower for variable width signed input.

Design
------

The window primitive contains a multiplier that multiplies ``data_in`` with ``coeff_in``. This is then divided by ``2^32`` (RSH 32) and round half up is the rounding method chosen.

.. _windower_primtive-diagram:

.. figure:: windower_primitive_block_diagram.svg
   :alt: Block diagram of windower primitive.
   :align: center

   Windower primitive implementation.


Implementation
--------------
The primitive is pipelined so that a new sample can be inserted into the primitive on every rising edge of ``clk``. The ``data_in_valid_in`` signal allows each input sample to be marked as valid or not. The pipeline advances every rising edge of ``clk`` regardless of if valid data is passed to the primitive or not. The ``data_out_valid_out`` signal is a delayed version of ``data_in_valid_in`` that indicates which outputs were calculated based on valid inputs.

The ``enable`` signal provides a clock enable for the primitive. When ``enable`` is set low no data will enter or leave the primitive and all calculations are stopped. When ``enable`` is high the module will operate normally.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g`` (``integer``): Bit width of ``data_in`` signal.

 * ``output_width_g`` (``integer``): Bit width of ``data_out`` signal.


Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. If ``enable`` is low the module will not operate.

 * ``data_in`` (``signed``, ``data_width_g`` bits), in: Sample input data into window function.

 * ``data_in_valid_in`` (``std_logic``), in: ``data_in`` is valid when ``data_in_valid_in`` is high.

 * ``data_out`` (``signed``, ``output_width_g`` bits), out: Output data from the window function.

 * ``data_out_valid_out`` (``std_logic``), out: ``data_out``  is valid when ``data_out_valid_out`` is high.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Half up rounder primitive <rounding_halfup-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
None.
