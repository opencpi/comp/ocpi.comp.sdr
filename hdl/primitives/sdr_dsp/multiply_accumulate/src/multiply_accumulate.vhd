-- HDL Implementation of a multiply accumulate unit.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity multiply_accumulate is
  generic (
    -- Max for Xilinx DSP48E1 slice is 25 x 18 multiply with 48bit accumulator
    -- Max for Cyclone/Arria V is 27 x 27 (or dual 18x19) multiply with
    -- 64bit accumulator.
    a_in_size_g        : integer := 16;
    b_in_size_g        : integer := 16;
    accumulator_size_g : integer := 48
    );
  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    en_in      : in  std_logic;
    a_in       : in  signed(a_in_size_g - 1 downto 0);
    b_in       : in  signed(b_in_size_g - 1 downto 0);
    load_accum : in  std_logic;
    accum_in   : in  signed(accumulator_size_g - 1 downto 0);
    accum_out  : out signed(accumulator_size_g - 1 downto 0)
    );
end multiply_accumulate;

architecture behavioural of multiply_accumulate is
  signal accum        : signed(accumulator_size_g - 1 downto 0);
  signal multi_res    : signed(a_in_size_g + b_in_size_g - 1 downto 0);
  signal load_accum_d : std_logic;
  signal accum_in_d   : signed(accumulator_size_g - 1 downto 0);
  signal en_in_d      : std_logic;
begin

  mac_p : process(clk)
  begin
    if rising_edge(clk) then
      if rst = '1' then
        accum        <= (others => '0');
        multi_res    <= (others => '0');
        load_accum_d <= '0';
        accum_in_d   <= (others => '0');
      else
        -- Create delayed control signals
        load_accum_d <= load_accum;
        accum_in_d   <= accum_in;
        en_in_d      <= en_in;
        -- Register multiply operation
        if en_in = '1' then
          multi_res <= a_in * b_in;
        end if;
        -- Also register sum operation
        if en_in_d = '1' then
          if load_accum_d = '1' then
            accum <= accum_in_d + multi_res;
          else
            accum <= accum + multi_res;
          end if;
        end if;
      end if;
    end if;
  end process;

  accum_out <= accum;

end behavioural;
