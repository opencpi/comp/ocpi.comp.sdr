.. ram_simple_dual_port documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _ram_simple_dual_port-primitive:


Simple Dual Port Block RAM (``ram_simple_dual_port``)
=====================================================
A RAM implementation, which allows selection of standard block RAM options.

Design
------
This is a RAM buffer that allows the data to be presented in a read first manner for simple dual port operation.

Implementation
--------------
The ``read_address`` input port is used to drive the index of the ``read_data`` port and the ``write_address`` is used to drive the index of the ``write_data`` port positions within the memory.

Inputs are written into the memory on the rising edge of ``clk`` when ``ram_enable`` and ``write_enable`` are both set. Outputs are read out of the memory on the rising edge of ``clk`` when ``ram_enable`` is set for a single stage pipeline. For multiple stage pipelines data propagates through the output pipeline when ``register_enable`` is set.

The primitive is configured in read first mode. This is where the previously written value is output if the same address is simultaneously being written to and read from.

The ``read_pipeline_g`` allows controlling of the number of register stages on the output of the read port of the RAM buffer. This provides control over the latency of the output, but also has a large impact on how easy the design can be routed. This defaults to 3, which is seen as a good balance and is generally capable of absorbing 2 into the RAM hardware on the FPGA, with the third registering any multiplexing required for large multi-block RAM memories).

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g``         (``integer``): The bit width of the ram and data ports, (defaults to 8).

 * ``buffer_depth_g``       (``integer``): The ram-depth of the primitive (in words of ``data_width_g``), (defaults to 16).

 * ``read_pipeline_g``      (``integer``): Read data pipeline length (defaults to 3).

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``ram_enable`` (``std_logic``), in: Enable RAM access when high.

 * ``register_enable`` (``std_logic``), in: Enable read data through output pipeline when high.

 * ``write_enable``  (``std_logic``), in : Data on ``write_data`` is written to the RAM (at address ``write_addr``) when asserted (together with ``ram_enable``).

 * ``write_address``    (``std_logic_vector``), in : Address to write a value into the RAM.

 * ``write_data``    (``std_logic_vector``), in : Data to write into RAM.

 * ``read_address``    (``std_logic_vector``), in : Address to read from within the RAM.

 * ``read_data``    (``std_logic_vector``), out: Data read from the ram following the pipeline.


Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``ram_simple_dual_port`` are:

 * None
