-- HDL Implementation of an updown counter.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Description :
-- A counter that can can count up or down.
-- It can load a value into the counter
-- -----------------------------------------------------------------------------
-- Usage :
-- If reset the counter value will be set to 0
-- To load a value into the counter:
--   Have the value on the load_value input and set the load triger to '1'
-- To increment the counter:
--   Have enable set to '1' and direction set to '1'
--   This will increment the counter every enabled clock pulse
-- To decrement the counter:
--   Have enable set to '1' and direction set to '0'
--   This will decrement the counter every enabled clock pulse
-- Loading a value will take priority over increment/decrement operations.
-- -----------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter_updown is
  generic (
    counter_size_g : integer := 8
    );
  port (
    clk        : in  std_logic;
    reset      : in  std_logic;
    enable     : in  std_logic;
    direction  : in  std_logic;
    load_trig  : in  std_logic;
    load_value : in  unsigned(counter_size_g - 1 downto 0);
    length     : in  unsigned(counter_size_g - 1 downto 0);
    count_out  : out unsigned(counter_size_g - 1 downto 0)
    );
end counter_updown;

architecture rtl of counter_updown is

  signal counter_value : unsigned(counter_size_g - 1 downto 0);

begin

  count_out <= counter_value;

  counter_p : process(clk)
  begin
    if rising_edge(clk) then
      if reset = '1' then
        counter_value <= (others => '0');
      -- Load new counter value
      elsif load_trig = '1' then
        counter_value <= load_value;
      elsif enable = '1' then
        -- Increment counter value
        if direction = '1' then
          if counter_value >= (length - 1) then
            counter_value <= (others => '0');
          else
            counter_value <= counter_value + 1;
          end if;
        -- Decrement counter value
        else
          if counter_value = 0 then
            counter_value <= (length - 1);
          elsif counter_value > (length - 1)then
            counter_value <= (length - 1);
          else
            counter_value <= counter_value - 1;
          end if;
        end if;
      end if;
    end if;
  end process;

end rtl;
