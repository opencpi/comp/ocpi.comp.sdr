.. cdc_pulse_handshake documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

.. _cdc_pulse_handshake-primitive:


Clock Domain Crossing - Pulse Handshake (``cdc_pulse_handshake``)
=================================================================
Pulse handshake clock domain crossing (CDC) synchroniser.

Design
------
Capture an active high pulse on the source clock domain and transfer it to a pulse on the destination clock domain, using clock domain crossing (CDC) synchronisers. Indicate to the source clock domain when it is ready to accept a new pulse input.

Implementation
--------------

If a pulse is received on the input signal (``src_pulse``) but the primitive is not ready to receive it (``src_ready`` is low) then the input signal will be ignored. Alternatively, if the primitive is ready, the ``src_toggle`` signal will be inverted and transferred through a :ref:`single bit synchroniser <cdc_single_bit-primitive>` from the source (``src_clk``) to the destination (``dst_clk``) clock domain to generate the ``dst_toggle`` signal. An edge detector will monitor this signal and generate a pulse on the output signal (``dst_pulse``) in the destination clock domain. The ``dst_toggle`` signal will be transferred through a :ref:`single bit synchroniser <cdc_single_bit-primitive>` back to the source clock domain to acknowledge that the pulse has been sent and indicate that the primitive is ready to receive another pulse on the input. The ``src_toggle`` signal will be held in a steady state, from when it is applied to the synchroniser until it is acknowledged, i.e. whilst the primitive is indicating it is not ready to receive a new input pulse. This ensures that the pulse will be transferred irrespective of the relationship between the rate of change of the input signal and the destination clock speed.

A block diagram representation of the implementation is given in :numref:`cdc_pulse_handshake-diagram`.

.. _cdc_pulse_handshake-diagram:

.. figure:: cdc_pulse_handshake.svg
   :alt: Block diagram of a pulse handshake clock domain crossing (CDC) synchroniser implementation
   :align: center

   Block diagram of a pulse handshake clock domain crossing (CDC) synchroniser implementation.

Further information on metastability and synchronisation can be found at:

 * http://www.sunburst-design.com/papers/CummingsSNUG2008Boston_CDC.pdf

 * https://www.edn.com/crossing-the-abyss-asynchronous-signals-in-a-synchronous-world/

The primitive passes signals between different clock domains and has been designed for that purpose. The primitive may contain attributes and should be paired with constraints to control their placement, optimising the MTBF through the synchronisers, and preventing the timing analyser from needlessly attempting to time the crossing signals. The Xilinx Vivado tool has the capability to report on the quality of the clock domain crossings in the design and can accept waivers for warnings generated for CDC architectures that it can not automatically verify. Platform/application developers should therefore consider adding the constraints, or similar if not using Vivado, to designs that instantiate the primitive. Refer to the documentation for all clock domain clocking primitives identified in the dependencies section, and all of their dependencies.

Interface
---------

Generics
~~~~~~~~

 * ``dst_registers_g``  (``natural``): Number (2 to 10) of synchronisation registers (defaults to 2). Increase for improved MTBF.

Ports
~~~~~

 * ``src_clk`` (``std_logic``), in: Source clock. Input registered on rising edge.

 * ``src_reset`` (``std_logic``), in: Source reset. Active high, synchronous with rising edge of source clock.

 * ``src_pulse`` (``std_logic``), in: Input active high pulse to be detected and resynchronised.

 * ``src_ready`` (``std_logic``), out: Output active high indicating ready to receive an input pulse on ``src_pulse``.

 * ``dst_clk`` (``std_logic``), in: Destination clock. Synchronisation registers and output registered on rising edge.

 * ``dst_reset`` (``std_logic``), in: Active high, synchronous with rising edge of clock.

 * ``dst_pulse`` (``std_logic``), out: Output active high pulse synchronised to the destination clock.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Clock Domain Crossing - Single bit primitive <cdc_single_bit-primitive>`.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cdc_pulse_handshake`` are:

 * None
