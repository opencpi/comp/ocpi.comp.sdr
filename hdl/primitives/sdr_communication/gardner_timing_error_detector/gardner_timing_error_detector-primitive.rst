.. gardner_timing_error_detector documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _gardner_timing_error_detector-primitive:


Gardner Timing Error Detector (TED) (``gardner_timing_error_detector``)
=======================================================================
Calculates the timing error within a zero crossing sample stream.

Design
------
This primitive implements a timing error detector with a common set of ports in order to allow the swapping out of error detectors.

The Gardner timing error detector works at twice the symbol rate, and outputs a signal that is proportional to the distance of the centre symbol from zero. An example of this can be seen in :numref:`gardner_early_late`, where :math:`N_3` is the centre symbol.

.. _gardner-sample-position-figure:

.. figure:: gardner_early_late.svg
   :name: gardner_early_late
   :alt: Sample position expectation within a Gardner loop.
   :align: center

   Sample position expectation within a Gardner loop

The Gardner timing error detection equation is given by :eq:`gardner_ted-equation`

.. math::
   :label: gardner_ted-equation

   TED = N_z \times (N_{z+1} - N_{z-1})

When synchronised, the centre sample :math:`N_z` is midway between between two symbols. An approximation of the derivative of the input samples is given by :math:`N_{z+1} - N_{z-1}` and this is then weighted by the actual centre sample value :math:`N_z`.

The performance of the error detection is impacted by the input samples passed into it and if the wrong ones are selected the calculated error may have the wrong sign. Also, if the input samples remain largely unchanged from one value to the next the calculated error can be misleading; a workaround that is implemented in this primitive is a check on the sign bits of the 3 previously received samples to determine whether the calculated error detection value is valid.

The error detector is designed to work only at x2 samples per symbol. Other sample-symbol rate ratios and sample selection thereof should be taken care of by the implementing component.

Implementation
--------------
The structure of the primitive is shown in :numref:`gardner-implementation`, with descriptions for the main blocks below.

.. figure:: gardner_implementation.svg
   :name: gardner-implementation
   :alt: Implementation of the Gardner timing error detector
   :align: center

   Block representation of the Gardner TED implementation.

The flow of data is controlled by ``input_valid`` being asserted. This primitive is designed to deliver an output sample for every 2 incoming samples.

The input samples are pushed into a serial load, parallel read shift register. The shift register is then read out for two purposes:

   * Calculation of the Gardner TED value, given by equation :eq:`gardner_ted-equation`

   * Gating the error detection calculation output, based on the signs (most significant bits) of the values in the shift register to check if a zero crossing is present within the data.

The sign bits (`sgn`) are used to ignore symbol runs, which do not want to be included within the error detection calculation. The logical decision whether as to whether the calculated error is valid is `(a&!c) | (!a&b) | (!b&c)`, where `a`, `b` and `c` are the most significant bits of `N[0]`, `N[1]` and `N[2]`, respectively.

``resync`` is used to resynchronise the primitive to ensure that the zero crossing is centred in the shift register `N[1]`, therefore ``resync`` should be asserted at the centre of a symbol. Once synchronised, a registered ``resync`` signal is inverted and every time ``input_valid`` is asserted this is used to gate the `output_valid` signal ensuring the error detection calculation output is valid at the right time.

Interface
---------

Generics
~~~~~~~~

 * ``data_width_g`` (``positive``): Width of the data words used within this primitive.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``resync`` (``std_logic``), in: Active high resynchronisation signal

 * ``input_valid`` (``std_logic``), in: Marks the incoming sample as valid

 * ``input_data`` (``std_logic_vector``, ``data_width_g`` bits), in: Input sample data to be processed (Signed data representation)

 * ``output_valid`` (``std_logic``), out: Asserted when a sample is available to be read (active for 1 cycle)

 * ``output_data`` (``std_logic_vector``, ``data_width_g``), out: Timing error estimation proportional to the incoming signal (Signed data representation)

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``gardner_timing_error_detector`` are:

 * After ``reset`` is asserted, a minimum of three samples must be clocked into the primitive before an output can be considered valid

 * The ``output_data`` error value is truncated to a length of ``data_width_g`` (by removing the lower ``data_width_g`` bits)
