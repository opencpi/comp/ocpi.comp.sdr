#!/usr/bin/env python3

# Signal manipulation functions
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

"""Signal manipulation functions."""

import numpy as np


def to_twos(value, bit_length):
    """Convert the passed value into a two's complement number.

    Args:
        value (int): the value to convert
        bit_length (int): the size of the two's complement number

    Returns:
        The converted value
    """
    if value > (2**(bit_length))-1 or value < (-2**(bit_length-1)):
        raise ValueError(
            "Error: Value {} is outside of integer input range [{}-{}]"
            .format(value, (-2**(bit_length-1)), (2**(bit_length-1))-1))
    value = value & ((1 << (bit_length))-1)
    if (value & (1 << (bit_length-1))) != 0:
        value = value - (1 << bit_length)
    if value > (2**(bit_length-1))-1 or value < (-2**(bit_length-1)):
        raise ValueError(
            "Error: Value {} is outside of integer input range [{}-{}]"
            .format(value, (-2**(bit_length-1)), (2**(bit_length-1))-1))
    return value
