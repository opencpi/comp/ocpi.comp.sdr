# OCPI Lint yaml configuration file
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# OCPI Lint yaml configuration file

# This file allows the customisation of the OpenCPI lint scripts.
# A project can contain multiple versions of this file, at different
# hierarchies.
# For example, there could be a version of this file at the project root, then
# an additional version within the hdl/primitives directory.
# Whether a configuration file inherits settings from parent directories is
# controlled via the "inherit_parent" setting.
# This can be set to "false" to *not* inherit settings, or
# to "true" (the default) to inherit settings from the parent directory.
inherit_parent: false

# Extra modules required in order to run the scripts present within this
# project.
# If the script is not installed then an error will be raised.
# If the script is installed, but not listed below then a warning will be
# raised.
extra_modules:
   - xml
   - lxml

# Extra rules defines a list of python files which should be read as
# containing rules or configurations.
# This could be a module path, or independent python files
extra_rules:
    - project_lint.py

# Lint classes defines a list of custom classes to use for linting. This
# can be used to replace existing checking classes, or to declare additional
# checkers.
lint_classes:
    # As this is a new checker, it is added with a unique name
    sdr_test_xml: TestXMLCodeChecker
    # As this is a new checker, it is added with a unique name
    sdr_makefiles: MakefileCodeChecker

# Enabled tests allows tests to be enabled for this project.
# Enabling is done through basic lint rule name matching
enable_tests:
    - any_000  # Check for trailing white space
    - any_001  # Check for default template code
    - any_002  # Check blank lines limit is not exceeded (default is 2)
    - any_003  # Check for single blank line at end of file
    - any_004  # Check spaces are used instead of tabs
    - cpp_000  # Run clang-format (Google style) over code
    - cpp_001  # Run cpplint over code
    - cpp_002  # Run cppcheck over code
    - cpp_003  # Check for existence of OpenCPI license header
    - cpp_004  # Check that block comments are not used
    - cpp_005  # Check #define is not used
    - cpp_006  # Check ".h>" C style headers are not used
    - cpp_007  # Check <iostream> is not used
    - cpp_008  # Check for 1) "//" header, 2) blank line, 3) #include
    - cpp_009  # Check that RCC_ERROR is not used
    - cpp_010  # Check for that setError is called after RCC_FATAL
    - cpp_011  # Check that there is only one class per file for a worker
    - cpp_012  # Check "malloc" and "free" are not used
    - cpp_013  # Check for use of non-OpenCPI types
    - py_000   # Run autopep8 over code
    - py_001   # Run pycodestyle over code
    - py_002   # Check for 1) hash-bang, 2) blank line, 3) single line comment,
               # 4)blank line, 5) OpenCPI license header, 6) blank line
    - py_003   # Check only double quotation marks are used
    - py_004   # Check only "complex()" is used over "j"
    - py_005   # Check docstrings are used (Google style)
    - rst_001  # Check for 1) comment, 2) blank line, 3) OpenCPI license header,
               # 4) blank line
    - rst_002  # Check lists, bullets and numbered, have blank lines between
    - rst_003  # Ensure underline lengths are the same as their header
    - vhdl_000 # Run emacs vhdl-format over code
    - vhdl_001 # Check for 1) comment, 2) blank line, 3) OpenCPI license header,
               # 4) blank line
    - vhdl_002 # Check for single space after "--" comment
    - vhdl_003 # Check comments do not exceed character limit (default is 80)
    - xml_000  # Run xmllint over code
    - xml_001  # Check for OpenCPI license header
    - xml_002  # Check only double quotation marks are used
    - xml_003  # Check comments do not exceed character limit (default is 80)
    - xml_004  # Check non-comments are all lowercase
    - xml_005  # Check "writable" attribute is explicitly set for all
               # properties in OCSs
    - xml_006  # Check "producer" attribute is explicitly set for all
               # ports in OCSs
    - xml_007  # Check "default" attribute is explicitly set for all
               # properties in OWDs
    - xml_008  # Not implemented
    - xml_009  # Check for <build> element in build file
    - xml_010  # Check OCS "componentspec" does not use "name" attribute
    - xml_011  # Check OCS order is 1) properties, 2) inputs, 3) outputs
    - xml_012  # Check "default" attribute is explicitly set for all
               # properties in OCSs
    - xml_013  # Check "description" attribute is explicitly set for all
               # properties in OCSs
    - yaml_000 # Check for 1) comment, 2) blank line, 3) OpenCPI license header,
               # 4) blank line
    - yaml_001 # Check for single space after "#" comment
    - yaml_002 # Check comments do not exceed character limit (default is 80)

# # Disabled tests allows existing tests to be skipped for this project.
# # Disabling is done through basic lint rule name matching
# disable_tests:
#   - any_000 # This will disable the single test called any_000

# Allows the ignoring of files and directories within the project.
# Standard path globbing wildcards are allowed.
# Note: that if the string doesn't start with an alphanumeric character then
#       the string should be wrapped in quotation marks
ignore_pattern:
    # Ignore these directories
    - "//*proxy.test/test_assemblies/"
    - "test_[0-9]*/" # Proxy have custom files currently
    - "__pycache__/"
    - ".pytest_cache/"
    # Ignore these filenames
    - LICENSE.txt
    - COPYRIGHT
    - CHANGELOG.md
    - Project.exports
    - specs/package-id
    - prerequisites/dblclockfft/install-dblclockfft.sh
    - prerequisites/kiss_fft/install-kiss_fft.sh
    # Ignore these extensions
    - "*.dict"
    - "*.input" # Proxy components have a fixed input file
